module.exports = {
    appId: 'education.lumi.lehrerwelt-player',
    productName: 'Player für interaktive Übungen', // DO NOT REPLACE THE ü or Ü in this line with one you type in the keyboard!!
    // It will break electron-builder and code-signing on macOS! You must copy a filename from Finder and paste it here!
    // Cp: https://github.com/electron-userland/electron-builder/issues/4932
    copyright:
        'Copyright © 2021 AAP Lehrerwelt GmbH (info@lehrerwelt.de, +49 40 325083-040)',
    asar: true,
    icon: 'electron/assets/logo.icns',
    files: [
        'build/**/*',
        'node_modules/**/*',
        'package.json',
        'h5p/**/*',
        'electron/**/*',
        'locales/**/*'
    ],
    mac: {
        category: 'public.app-category.education',
        gatekeeperAssess: false,
        entitlements: 'electron/mac/entitlements.mac.plist',
        entitlementsInherit: 'electron/mac/entitlements.mac.plist',
        artifactName:
            'Player-fuer-interaktive-Uebungen-${version}-${arch}.${ext}',
        fileAssociations: {
            ext: 'h5p',
            name: 'H5P'
        },
        target: [
            {
                target: 'dmg',
                arch: ['arm64', 'x64']
            },
            {
                target: 'zip',
                arch: ['arm64', 'x64']
            }
        ],
        hardenedRuntime: true
    },
    afterSign: 'scripts/notarize.js',
    win: {
        icon: 'electron/assets/logo.ico',
        target: [{ target: 'nsis', arch: 'x64' }],
        fileAssociations: {
            ext: 'h5p',
            name: 'H5P'
        },
        certificateSubjectName: 'AAP Lehrerwelt GmbH',
        certificateSha1: '462B894AA055965C6EA3DE1ACEBF54646A68B631',
        artifactName: 'Player-für-interaktive-Übungen-${version}.${ext}'
    },
    nsis: {
        deleteAppDataOnUninstall: true
    },
    dmg: {
        sign: false
    },
    publish: {
        provider: 'bitbucket',
        owner: 'aap-lehrerwelt',
        slug: 'aap-lehrerwelt-player',
        publishAutoUpdate: true
    }
};

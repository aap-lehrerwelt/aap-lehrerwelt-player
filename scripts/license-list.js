/**
 * Generates a list of the licenses used in the application.
 */
const fs = require('fs');
const path = require('path');
const { encode } = require('html-entities');
const checker = require('license-checker-rseidelsohn');

function output(packages) {
    const packagesWithCleanNames = Object.keys(packages).reduce((curr, key) => {
        curr[key.substr(0, key.lastIndexOf('@'))] = packages[key];
        return curr;
    }, {});

    const licenses = Object.keys(packagesWithCleanNames)
        .map((name) => {
            const package = packagesWithCleanNames[name];
            const licenseText = encode(
                package.licenseText ||
                    (package.licenseFile &&
                        !package.licenseFile.toLowerCase().includes('readme'))
                    ? fs.readFileSync(package.licenseFile, 'utf-8')
                    : undefined
            );
            let text = `<p><b>${encode(name)}</b>`;
            if (package.publisher) text += ' von ' + encode(package.publisher);
            if (package.email) text += ' (' + encode(package.email) + ')';
            text += ' lizensiert unter ' + encode(package.licenses);
            if (package.repository)
                text += `, herunterladbar unter <a href="${package.repository}">${package.repository}</a>`;
            if (licenseText) {
                text +=
                    '<details><summary>vollst&auml;ndiger Lizenztext</summary>';
                text += licenseText.replaceAll('\n', '<br/>');
                text += '</details>';
            }
            text += '</p>';
            return text;
        })
        .filter((l) => l !== undefined);

    const fileContent = `
    <html>
    <head>
    <title>Lizenzinformation</title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }
        h1 {
            text-align: center;
        }
    </style>
    </head>
    <body>    
        <h1>Lizenzinformationen von Drittkomponenten</h1>
        <p><b>Diese Anwendung enth&auml;lt Drittkomponenten unter folgenden Lizenzen:</b><p>
        <p><b>h5p-php-library</b> von Joubel und anderen Beitragenden lizensiert unter GPL 3.0, herunterladbar unter <a href="https://github.com/h5p/h5p-php-library">https://github.com/h5p/h5p-php-library</a>
        <details><summary>vollst&auml;ndiger Lizenztext</summary>"
        ${encode(
            fs.readFileSync(
                path.resolve(__dirname, '../h5p/core/LICENSE.txt'),
                'utf-8'
            )
        ).replaceAll('\n', '<br/>')}
        </details>
        </p>
        <hr/>
        ${licenses.join('<hr/>')}
    </body>
    </html>`;

    const dir = './client/public';

    if (!fs.existsSync(dir)) fs.mkdirSync(dir);

    fs.writeFile(`${dir}/DISCLAIMER.html`, fileContent, (err) => {
        if (err) {
            console.error(
                'ERROR: DISCLAIMER.html file has NOT been created.\n',
                err,
                '\n'
            );
            return err;
        }
        console.info(
            `OK: ${dir}/DISCLAIMER.html file with ${licenses.length} licenses has been created.\n`
        );
    });
}

// Temporarily move electron and electron-builder to production dependencies, as
// they are included in the bundle but can't be in the dependencies list.

const packagePath = path.resolve(__dirname, '../package.json');

const oldPackage = fs.readFileSync(packagePath, 'utf-8');

const oldPackageJson = JSON.parse(oldPackage);
oldPackageJson.dependencies['electron'] =
    oldPackageJson.devDependencies['electron'];
oldPackageJson.dependencies['electron-builder'] =
    oldPackageJson.devDependencies['electron-builder'];
fs.writeFileSync(packagePath, JSON.stringify(oldPackageJson));

checker.init(
    {
        start: path.resolve(__dirname, '..'),
        production: true
    },
    function (err, packages) {
        if (err) {
            console.error(err);
        } else {
            checker.init(
                {
                    start: path.resolve(__dirname, '../client'),
                    production: true
                },
                function (err2, packages2) {
                    if (err2) {
                        console.error(err);
                    } else {
                        output({ ...packages, ...packages2 });
                        fs.writeFileSync(packagePath, oldPackage, 'utf8');
                    }
                }
            );
        }
    }
);

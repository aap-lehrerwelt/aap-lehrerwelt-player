# Needed to create fully funtioning DMG and zip files for macOS. You may need to
# allow the execution of the downloaded 7zip program in the security settings of
# system control.

# Uploads to Bitbucket can run into a timeout otherwise
sed -i '' 's/setTimeout(60 \* 1000/setTimeout(20 * 60 * 1000/g' node_modules/builder-util-runtime/out/httpExecutor.js      

# Update to newer 7-zip version (needed für ü and Ü in app name)
wget -qO- https://www.7-zip.org/a/7z2103-mac.tar.xz | tar zxvf - 7zz
cp ./7zz node_modules/7zip-bin/mac/arm64/7za
mv ./7zz node_modules/7zip-bin/mac/x64/7za
sed -i '' 's/args.push("-mcu");$/args.push("-mcu");args.push("-snl")/g' node_modules/app-builder-lib/out/targets/archive.js
import * as Sentry from '@sentry/browser';

import { ISystemInformation } from '../../services/systemInfo';
import { ISystemActionTypes, SYSTEM_GET_SYSTEM_SUCCESS } from './SystemTypes';

export const initialState: ISystemInformation = {
    platformSupportsUpdates: true,
    platform: 'mac'
};

export default function settingsReducer(
    state: ISystemInformation = initialState,
    action: ISystemActionTypes
): ISystemInformation {
    try {
        switch (action.type) {
            case SYSTEM_GET_SYSTEM_SUCCESS:
                return action.payload;

            default:
                return state;
        }
    } catch (error) {
        Sentry.captureException(error);
        return state;
    }
}

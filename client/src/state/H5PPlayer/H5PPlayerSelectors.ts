import Logger from '../../helpers/Logger';
import * as Sentry from '@sentry/browser';

import { IContent, IState, WidthLimits } from './H5PPlayerTypes';

const log = new Logger('selectors:player');

export const errorObject: IContent = {
    contentId: 'new',
    loadingIndicator: false,
    mainLibrary: '',
    name: 'error',
    viewDisabled: true,
    opening: false,
    widthLimit: WidthLimits.None,
    zoomLevel: 1
};

export function activeContent(state: IState): IContent {
    try {
        log.debug(`selecting activeContent`);
        return state.h5pPlayer.content || errorObject;
    } catch (error) {
        Sentry.captureException(error);

        log.error(error);
        return errorObject;
    }
}

export function noActiveContent(state: IState): boolean {
    try {
        return state.h5pPlayer.content === undefined;
    } catch (error) {
        Sentry.captureException(error);

        log.error(error);
        return false;
    }
}

export function viewDisabled(state: IState): boolean {
    try {
        return state.h5pPlayer.content?.viewDisabled === true;
    } catch (error) {
        return true;
    }
}

export function error(state: IState):
    | {
          details?: string;
          message?: string;
      }
    | undefined {
    if (!state.h5pPlayer.errorMessage) {
        return undefined;
    }
    return {
        message: state.h5pPlayer.errorMessage,
        details: state.h5pPlayer.errorDetails
    };
}

export function windowId(state: IState): string {
    return state.h5pPlayer.windowId;
}

export function zoomLevel(state: IState): number {
    return state.h5pPlayer.content?.zoomLevel ?? 1;
}

export function widthLimit(state: IState): WidthLimits {
    return state.h5pPlayer.content?.widthLimit ?? WidthLimits.None;
}

export function isPrivacyBlocked(state: IState): boolean {
    const mainLibrary = state.h5pPlayer.content?.mainLibrary;
    return (
        mainLibrary !== undefined &&
        (mainLibrary.startsWith('H5P.SpeakTheWords') ||
            mainLibrary.startsWith('H5P.SpeakTheWordsSet'))
    );
}

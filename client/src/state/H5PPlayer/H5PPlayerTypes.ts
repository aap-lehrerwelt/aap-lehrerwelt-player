import { IPlayerModel, IContentMetadata } from '@lumieducation/h5p-server';
import Superagent from 'superagent';

// types

export type ContentId = string;

export interface IH5P {
    id: ContentId;
    library: string;
    metadata: IContentMetadata;
    parameters: any;
}

export const SHOW_SETTINGS = 'SHOW_SETTINGS';

export interface IShowSettings {
    type: typeof SHOW_SETTINGS;
}
export const H5P_PLAYER_OPEN = 'H5P_PLAYER_OPEN';
export const H5P_PLAYER_LOADED = 'H5P_PLAYER_LOADED';
export const H5P_PLAYER_INITIALIZED = 'H5P_PLAYER_INITIALIZED';

export interface IContent {
    contentId?: ContentId;
    loadingIndicator: boolean;
    mainLibrary: string;
    name: string;
    opening: boolean;
    path?: string;
    viewDisabled: boolean;
    widthLimit: WidthLimits;
    zoomLevel: number;
}

export enum WidthLimits {
    None = 0,
    Narrow = 1,
    Medium = 2,
    Wide = 3,
    ExtraWide = 4
}

export interface IH5PPlayerState {
    content?: IContent;
    errorDetails?: string;
    errorMessage?: string;
    lockDisplay: boolean;
    settingsSection: 'general' | 'updates';
    showExportDialog: boolean;
    showSettings: boolean;
    windowId: string;
}

export interface IState {
    h5pPlayer: IH5PPlayerState;
}

export interface IH5POpenAction {
    payload: {
        content?: Partial<IContent>;
        id: string;
    };
    type: typeof H5P_PLAYER_OPEN;
}

export type H5PPlayerActionTypes =
    | IShowSettingsAction
    | IH5POpenAction
    | IH5PLoadPlayerContentRequestAction
    | IH5PLoadPlayerContentSuccessAction
    | IH5PImportRequestAction
    | IH5PImportSuccessAction
    | IH5PImportErrorAction
    | IH5PZoomInAction
    | IH5PZoomOutAction
    | IH5PZoomResetAction
    | IH5PSetWidthLimitAction;

export const H5P_PLAYER_ERROR = 'H5P_PLAYER_ERROR';

export interface IH5PPlayerError {
    payload: {
        message: string;
    };
    type: typeof H5P_PLAYER_ERROR;
}

export const H5P_LOADPLAYERCONTENT_REQUEST = 'H5P_LOADPLAYERCONTENT_REQUEST';
export const H5P_LOADPLAYERCONTENT_SUCCESS = 'H5P_LOADPLAYERCONTENT_SUCCESS';
export const H5P_LOADPLAYERCONTENT_ERROR = 'H5P_LOADPLAYERCONTENT_ERROR';

export interface IShowSettingsAction {
    payload: {
        show: boolean;
        section: 'general' | 'updates';
    };
    type: typeof SHOW_SETTINGS;
}

export interface IH5PLoadPlayerContentRequestAction {
    payload: {
        contentId: ContentId;
    };
    type: typeof H5P_LOADPLAYERCONTENT_REQUEST;
}

export interface IH5PLoadPlayerContentErrorAction {
    payload: {
        contentId: ContentId;
    };
    type: typeof H5P_LOADPLAYERCONTENT_ERROR;
}

export interface IH5PLoadPlayerContentSuccessAction {
    payload: {
        content: IPlayerModel;
        contentId: ContentId;
    };
    type: typeof H5P_LOADPLAYERCONTENT_SUCCESS;
}

export type LoadPlayerContentActions =
    | IH5PLoadPlayerContentErrorAction
    | IH5PLoadPlayerContentRequestAction
    | IH5PLoadPlayerContentSuccessAction;

// Import

export type ImportActions =
    | IH5PImportRequestAction
    | IH5PImportErrorAction
    | IH5PImportSuccessAction;

export const H5P_IMPORT_ERROR = 'H5P_IMPORT_ERROR';
export const H5P_IMPORT_REQUEST = 'H5P_IMPORT_REQUEST';
export const H5P_IMPORT_SUCCESS = 'H5P_IMPORT_SUCCESS';

export interface IH5PImportErrorAction {
    error: { response: Superagent.Response };
    payload: {
        details: string;
        message: string;
        path: string;
    };
    type: typeof H5P_IMPORT_ERROR;
}
export interface IH5PImportRequestAction {
    payload: {
        path: string;
    };
    type: typeof H5P_IMPORT_REQUEST;
}

export interface IH5PImportSuccessAction {
    payload: {
        h5p: IH5P;
        path: string;
    };
    type: typeof H5P_IMPORT_SUCCESS;
}

export const H5P_ZOOM_IN = 'H5P_ZOOM_IN';
export const H5P_ZOOM_OUT = 'H5P_ZOOM_OUT';
export const H5P_ZOOM_RESET = 'H5P_ZOOM_RESET';

export interface IH5PZoomInAction {
    payload: {};
    type: typeof H5P_ZOOM_IN;
}

export interface IH5PZoomOutAction {
    payload: {};
    type: typeof H5P_ZOOM_OUT;
}

export interface IH5PZoomResetAction {
    payload: {};
    type: typeof H5P_ZOOM_RESET;
}

export const H5P_SET_WIDTH_LIMIT = 'H5P_SET_WIDTH_LIMIT';

export interface IH5PSetWidthLimitAction {
    payload: {
        limit: WidthLimits;
    };
    type: typeof H5P_SET_WIDTH_LIMIT;
}

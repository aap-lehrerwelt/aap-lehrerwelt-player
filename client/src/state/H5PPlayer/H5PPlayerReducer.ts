import Logger from '../../helpers/Logger';
import * as Sentry from '@sentry/browser';
import i18next from 'i18next';
import path from 'path';
import qs from 'qs';

import {
    IH5PPlayerState,
    H5PPlayerActionTypes,
    H5P_PLAYER_OPEN,
    H5P_LOADPLAYERCONTENT_REQUEST,
    H5P_LOADPLAYERCONTENT_SUCCESS,
    H5P_IMPORT_REQUEST,
    H5P_IMPORT_SUCCESS,
    H5P_IMPORT_ERROR,
    H5P_ZOOM_RESET,
    H5P_ZOOM_IN,
    H5P_ZOOM_OUT,
    WidthLimits,
    H5P_SET_WIDTH_LIMIT,
    SHOW_SETTINGS
} from './H5PPlayerTypes';

export const initialState: IH5PPlayerState = {
    errorDetails: undefined,
    errorMessage: undefined,
    lockDisplay: false,
    showExportDialog: false,
    content: undefined,
    windowId: qs.parse(window.location.search.replace('?', ''))
        .windowId as string,
    showSettings: false,
    settingsSection: 'general'
};

/**
 * Sets default width limits for all known content types
 */
const defaultWidthLimit = (mainLibrary: string): WidthLimits => {
    const machineName = mainLibrary.substr(0, mainLibrary.indexOf(' '));
    switch (machineName) {
        case 'H5P.Audio':
        case 'H5P.AudioRecorder':
        case 'H5P.Column':
        case 'H5P.Dialogcards':
        case 'H5P.GreetingCard':
        case 'H5P.KewarCode':
            return WidthLimits.Narrow;

        case 'H5P.Accordion':
        case 'H5P.AdvancedBlanks':
        case 'H5P.AdventCalendar':
        case 'H5P.Agamotto':
        case 'H5P.Bingo':
        case 'H5P.Blanks':
        case 'H5P.Chart':
        case 'H5P.Collage':
        case 'H5P.Cornell':
        case 'H5P.Crossword':
        case 'H5P.Dictation':
        case 'H5P.DocumentationTool':
        case 'H5P.DragText':
        case 'H5P.Essay':
        case 'H5P.FacebookFeedPage':
        case 'H5P.FindTheWords':
        case 'H5P.GuessTheAnswer':
        case 'H5P.ImagePair':
        case 'H5P.InteractiveBook':
        case 'H5P.MarkTheWords':
        case 'H5P.MemoryGame':
        case 'H5P.MultiChoice':
        case 'H5P.PersonalityQuiz':
        case 'H5P.PickTheSymbols':
        case 'H5P.Questionnaire':
        case 'H5P.QuestionSet':
        case 'H5P.SingleChoiceSet':
        case 'H5P.SortParagraphs':
        case 'H5P.SpeakTheWords':
        case 'H5P.SpeakTheWordsSet':
        case 'H5P.Summary':
        case 'H5P.TrueFalse':
        case 'H5P.TwitterUserFeed':
            return WidthLimits.Medium;

        case 'H5P.ArithmeticQuiz':
        case 'H5P.ArScavenger':
        case 'H5P.BranchingScenario':
        case 'H5P.CoursePresentation':
        case 'H5P.DragQuestion':
        case 'H5P.Flashcards':
        case 'H5P.ImageHotspotQuestion':
        case 'H5P.ImageHotspots':
        case 'H5P.ImageJuxtaposition':
        case 'H5P.ImageMultipleHotspotQuestion':
        case 'H5P.ImageSequencing':
        case 'H5P.ImageSlider':
        case 'H5P.ImpressivePresentation':
        case 'H5P.InteractiveVideo':
        case 'H5P.MultiMediaChoice':
        case 'H5P.ThreeImage':
        case 'H5P.Timeline':
            return WidthLimits.Wide;

        default:
            return WidthLimits.Medium;
    }
};

const log = new Logger('reducer:player');

export default function playerReducer(
    state: IH5PPlayerState = initialState,
    action: H5PPlayerActionTypes
): IH5PPlayerState {
    try {
        log.debug(`reducing ${action.type}`);
        switch (action.type) {
            case SHOW_SETTINGS:
                return {
                    ...state,
                    showSettings: action.payload.show,
                    settingsSection: action.payload.section
                };
            case H5P_LOADPLAYERCONTENT_REQUEST:
                return {
                    ...state,
                    content:
                        state.content === undefined
                            ? undefined
                            : {
                                  ...state.content,
                                  loadingIndicator: true
                              }
                };

            case H5P_LOADPLAYERCONTENT_SUCCESS:
                return {
                    ...state,
                    content:
                        state.content === undefined
                            ? undefined
                            : {
                                  ...state.content,
                                  loadingIndicator: false
                              }
                };

            case H5P_IMPORT_REQUEST:
                return {
                    ...state,
                    errorDetails: undefined,
                    errorMessage: undefined,
                    content: {
                        path: action.payload.path,
                        loadingIndicator: true,
                        viewDisabled: true,
                        mainLibrary: '',
                        name: path.basename(action.payload.path),
                        opening: true,
                        zoomLevel: 1,
                        widthLimit: WidthLimits.Narrow
                    }
                };

            case H5P_IMPORT_SUCCESS:
                return {
                    ...state,
                    errorDetails: undefined,
                    errorMessage: undefined,
                    content: {
                        path: action.payload.path,
                        contentId: action.payload.h5p.id,
                        loadingIndicator: false,
                        viewDisabled: false,
                        mainLibrary: action.payload.h5p.library,
                        name: action.payload.h5p.metadata.title,
                        opening: false,
                        zoomLevel: 1,
                        widthLimit: defaultWidthLimit(
                            action.payload.h5p.library
                        )
                    }
                };

            case H5P_IMPORT_ERROR:
                return {
                    ...state,
                    errorDetails: action.payload.details,
                    errorMessage: action.payload.message
                };

            case H5P_PLAYER_OPEN:
                return {
                    ...state,
                    content: {
                        loadingIndicator: true,
                        viewDisabled: true,
                        mainLibrary: '',
                        name: i18next.t('player.defaultName'),
                        path: undefined,
                        opening: false,
                        zoomLevel: 1,
                        widthLimit: WidthLimits.Narrow,
                        ...action.payload.content
                    }
                };

            case H5P_ZOOM_RESET:
                return {
                    ...state,
                    content: !state.content
                        ? undefined
                        : {
                              ...state.content,
                              zoomLevel: 1
                          }
                };

            case H5P_ZOOM_IN:
                return {
                    ...state,
                    content: !state.content
                        ? undefined
                        : {
                              ...state.content,
                              zoomLevel:
                                  state.content.zoomLevel >= 2.0
                                      ? state.content.zoomLevel
                                      : state.content.zoomLevel + 0.125
                          }
                };

            case H5P_ZOOM_OUT:
                return {
                    ...state,
                    content: !state.content
                        ? undefined
                        : {
                              ...state.content,
                              zoomLevel:
                                  state.content.zoomLevel <= 0.125
                                      ? state.content.zoomLevel
                                      : state.content.zoomLevel - 0.125
                          }
                };

            case H5P_SET_WIDTH_LIMIT:
                return {
                    ...state,
                    content: !state.content
                        ? undefined
                        : {
                              ...state.content,
                              widthLimit:
                                  action.payload.limit ?? WidthLimits.None
                          }
                };

            default:
                return state;
        }
    } catch (error) {
        Sentry.captureException(error);

        log.error(error);
        return state;
    }
}

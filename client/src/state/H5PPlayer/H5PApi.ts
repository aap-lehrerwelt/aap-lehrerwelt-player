import superagent from 'superagent';

export function loadPlayerContent(
    contentId: string
): Promise<superagent.Response> {
    return superagent.get(`/api/v1/h5p/${contentId}/play`);
}

export function openFiles(): Promise<superagent.Response> {
    return superagent.get('/api/v1/lumi/selectFiles');
}

export function importH5P(
    path: string,
    windowId: string,
    activeContentId: string | undefined
): Promise<superagent.Response> {
    return superagent.post(`/api/v1/lumi/import`).send({
        path,
        windowId,
        activeContentId
    });
}

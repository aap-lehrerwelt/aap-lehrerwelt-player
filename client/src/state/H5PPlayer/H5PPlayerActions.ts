import { ResponseError } from 'superagent';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import * as H from 'history';
import * as Sentry from '@sentry/browser';
import { decode } from 'html-entities';

import {
    ContentId,
    SHOW_SETTINGS,
    H5P_IMPORT_ERROR,
    H5P_IMPORT_REQUEST,
    H5P_IMPORT_SUCCESS,
    H5P_LOADPLAYERCONTENT_REQUEST,
    H5P_LOADPLAYERCONTENT_SUCCESS,
    H5P_PLAYER_INITIALIZED,
    ImportActions,
    LoadPlayerContentActions,
    IState,
    H5P_ZOOM_IN,
    H5P_ZOOM_OUT,
    H5P_ZOOM_RESET
} from './H5PPlayerTypes';

import * as api from './H5PApi';
import i18next from 'i18next';
import { IPlayerModel } from '@lumieducation/h5p-server';
import {
    activeContent as getActiveContent,
    windowId as getWindowId
} from './H5PPlayerSelectors';

export function showUpdatesSettings(): any {
    return {
        payload: {
            show: true,
            section: 'updates'
        },
        type: SHOW_SETTINGS
    };
}

export function showGeneralSettings(): any {
    return {
        payload: {
            show: true,
            section: 'general'
        },
        type: SHOW_SETTINGS
    };
}

export function hideSettings(): any {
    return {
        payload: {
            show: false
        },
        type: SHOW_SETTINGS
    };
}

export function playerInitialized(): any {
    return {
        payload: {},
        type: H5P_PLAYER_INITIALIZED
    };
}

export function openFileDialog(): ThunkAction<void, IState, null, any> {
    return (dispatch: ThunkDispatch<null, null, any>) => {
        api.openFiles()
            .then((response) => {
                const files = response.body;
                files.forEach((file: string) => {
                    dispatch(importH5P(file));
                });
            })
            .catch((error) => Sentry.captureException(error));
        return dispatch;
    };
}

export function loadPlayerContent(
    contentId: ContentId
): ThunkAction<Promise<IPlayerModel>, null, null, LoadPlayerContentActions> {
    if (!contentId) {
        throw new Error('no contentId');
    }
    return async (
        dispatch: ThunkDispatch<null, null, LoadPlayerContentActions>
    ) => {
        dispatch({
            payload: { contentId },
            type: H5P_LOADPLAYERCONTENT_REQUEST
        });

        const content = JSON.parse(
            (await api.loadPlayerContent(contentId)).text
        );

        dispatch({
            payload: { contentId, content },
            type: H5P_LOADPLAYERCONTENT_SUCCESS
        });

        return content;
    };
}

export function importH5P(
    path: string,
    history?: H.History
): ThunkAction<void, IState, null, ImportActions> {
    return async (dispatch: any, getState) => {
        const state = getState();
        const windowId = getWindowId(state);
        const activeContent = getActiveContent(state);

        dispatch({
            payload: { path },
            type: H5P_IMPORT_REQUEST
        });

        if (history) {
            history.push('/h5pPlayer');
        }

        try {
            const result = await api.importH5P(
                path,
                windowId,
                activeContent?.contentId === 'new'
                    ? undefined
                    : activeContent?.contentId
            );
            dispatch({
                payload: { path, h5p: result.body },
                type: H5P_IMPORT_SUCCESS
            });
        } catch (error) {
            Sentry.captureException(error);
            if (error.response) {
                const responseError = error as ResponseError;
                let details = '';
                if (
                    responseError.response?.body.errorCode ===
                    'VALIDATION_FAILED'
                ) {
                    details = responseError.response?.body?.message ?? '';
                    if (
                        details &&
                        details !== '' &&
                        responseError.response?.body?.details?.length > 0
                    ) {
                        details +=
                            ' ' +
                            responseError.response?.body?.details
                                ?.map((d: any) => decode(d.message))
                                .join(' ');
                    }
                }
                dispatch({
                    responseError,
                    payload: {
                        details,
                        path,
                        message: i18next.t('notifications.h5pPlayer.open.error')
                    },
                    type: H5P_IMPORT_ERROR
                });
            }
        }
    };
}

export function zoomIn(): (dispatch: any) => void {
    return (dispatch: any) => {
        dispatch({
            payload: {},
            type: H5P_ZOOM_IN
        });
    };
}

export function zoomOut(): (dispatch: any) => void {
    return (dispatch: any) => {
        dispatch({
            payload: {},
            type: H5P_ZOOM_OUT
        });
    };
}

export function zoomReset(): (dispatch: any) => void {
    return (dispatch: any) => {
        dispatch({
            payload: {},
            type: H5P_ZOOM_RESET
        });
    };
}

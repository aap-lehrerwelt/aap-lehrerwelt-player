import * as Sentry from '@sentry/browser';
import {
    CLOSE_SNACKBAR,
    ENQUEUE_SNACKBAR,
    INotificationsState,
    REMOVE_SNACKBAR,
    INotifyAction,
    ICloseSnackbar,
    IRemoveSnackbar,
    SHOW_ERROR_DIALOG,
    IShowErrorDialog,
    ICloseErrorDialog,
    CLOSE_ERROR_DIALOG
} from './NotificationsTypes';

import {
    H5P_PLAYER_ERROR,
    IH5PPlayerError,
    IH5PImportErrorAction
} from '../H5PPlayer/H5PPlayerTypes';

import shortid from 'shortid';

export const initialState: INotificationsState = {
    notifications: [],
    showErrorDialog: false,
    error: {
        code: 'init',
        message: ''
    }
};

export default function notificationsReducer(
    state: INotificationsState = initialState,
    action:
        | INotifyAction
        | ICloseSnackbar
        | IRemoveSnackbar
        | IH5PPlayerError
        | IH5PImportErrorAction
        | IShowErrorDialog
        | ICloseErrorDialog
): INotificationsState {
    try {
        switch (action.type) {
            case SHOW_ERROR_DIALOG:
                return {
                    ...state,
                    showErrorDialog: true,
                    error: action.payload.error
                };

            case CLOSE_ERROR_DIALOG:
                return {
                    ...state,
                    error: {
                        code: 'init',
                        message: '',
                        redirect: undefined
                    },
                    showErrorDialog: false
                };

            case H5P_PLAYER_ERROR:
                return {
                    ...state,
                    notifications: [
                        ...state.notifications,
                        {
                            key: shortid(),
                            message: action.payload.message,
                            options: {
                                variant: 'warning'
                            }
                        }
                    ]
                };

            case ENQUEUE_SNACKBAR:
                return {
                    ...state,
                    notifications: [
                        ...state.notifications,
                        {
                            ...action.notification
                        }
                    ]
                };

            case CLOSE_SNACKBAR:
                return {
                    ...state,
                    notifications: state.notifications.map(
                        (notification: any) =>
                            action.dismissAll || notification.key === action.key
                                ? { ...notification, dismissed: true }
                                : { ...notification }
                    )
                };

            case REMOVE_SNACKBAR:
                return {
                    ...state,
                    notifications: state.notifications.filter(
                        (notification: any) => notification.key !== action.key
                    )
                };

            default:
                return state;
        }
    } catch (error) {
        Sentry.captureException(error);
        return state;
    }
}

import { combineReducers, applyMiddleware, compose, createStore } from 'redux';
import * as Sentry from '@sentry/react';

import * as NotificationsActions from './Notifications/NotificationsActions';
import NotificationsReducer from './Notifications/NotificationsReducer';
import * as H5PPlayerActions from './H5PPlayer/H5PPlayerActions';
import * as H5PPlayerTypes from './H5PPlayer/H5PPlayerTypes';
import H5PPlayerReducer from './H5PPlayer/H5PPlayerReducer';
import * as NotificationsSelectors from './Notifications/NotificationsSelectors';
import * as H5PPlayerSelector from './H5PPlayer/H5PPlayerSelectors';

import * as NotificationsTypes from './Notifications/NotificationsTypes';

import * as SettingsTypes from './Settings/SettingsTypes';
import SettingsReducer from './Settings/SettingsReducer';
import * as SettingsActions from './Settings/SettingsActions';

import * as SystemTypes from './System/SystemTypes';
import SystemReducer from './System/SystemReducer';
import * as SystemActions from './System/SystemActions';

import * as UpdatesTypes from './Updates/UpdatesTypes';
import UpdatesReducer from './Updates/UpdatesReducer';
import * as UpdatesActions from './Updates/UpdatesActions';

import thunk from 'redux-thunk';

import Logger from '../helpers/Logger';
const log = new Logger('store');

declare var window: any;

const persistentState = undefined;

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

log.info(`initializing store`);

const sentryReduxEnhancer = Sentry.createReduxEnhancer({
    // Optionally pass options listed below
});

const middleWares = [thunk];

// state - reducer
const rootReducer = () =>
    combineReducers({
        notifications: NotificationsReducer,
        h5pPlayer: H5PPlayerReducer,
        settings: SettingsReducer,
        system: SystemReducer,
        updates: UpdatesReducer
    });

const store = createStore(
    rootReducer(),
    persistentState,
    composeEnhancers(applyMiddleware(...middleWares), sentryReduxEnhancer)
);

export interface IState
    extends H5PPlayerTypes.IState,
        NotificationsTypes.IState,
        SettingsTypes.IState,
        SystemTypes.IState,
        UpdatesTypes.IState {}

export const actions = {
    notifications: NotificationsActions,
    h5pPlayer: H5PPlayerActions,
    settings: SettingsActions,
    system: SystemActions,
    updates: UpdatesActions
};

export const selectors = {
    notifications: NotificationsSelectors,
    h5pPlayer: H5PPlayerSelector
};

export default store;

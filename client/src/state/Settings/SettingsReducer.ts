import * as Sentry from '@sentry/browser';

import {
    ISettingsActionTypes,
    ISettingsState,
    SETTINGS_GET_SETTINGS_SUCCESS,
    SETTINGS_CHANGE,
    SETTINGS_UPDATE_SUCCESS
} from './SettingsTypes';

export const initialState: ISettingsState = {
    areLoaded: false,
    allowPrerelease: false,
    autoUpdates: false,
    firstOpen: false,
    language: 'de',
    lastVersion: '',
    licenseConsent: false
};

export default function settingsReducer(
    state: ISettingsState = initialState,
    action: ISettingsActionTypes
): ISettingsState {
    try {
        switch (action.type) {
            case SETTINGS_GET_SETTINGS_SUCCESS:
                return { ...action.payload, areLoaded: true };

            case SETTINGS_CHANGE:
                return {
                    ...state,
                    ...action.payload,
                    areLoaded: true
                };

            case SETTINGS_UPDATE_SUCCESS:
                return { ...action.payload, areLoaded: true };

            default:
                return state;
        }
    } catch (error) {
        Sentry.captureException(error);
        return state;
    }
}

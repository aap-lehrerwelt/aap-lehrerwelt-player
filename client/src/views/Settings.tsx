import React from 'react';
import classnames from 'classnames';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Slide from '@material-ui/core/Slide';
import { TransitionProps } from '@material-ui/core/transitions';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import CloseIcon from '@material-ui/icons/Close';
import SettingsIcon from '@material-ui/icons/Settings';
import UpdateIcon from '@material-ui/icons/Update';
import UpdateSettings from './components/Settings/UpdatesSettings';
import LinkList from './components/Settings/LinkList';

import * as actions from '../state/H5PPlayer/H5PPlayerActions';

import { IState } from '../state';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appBar: {
            position: 'relative'
        },
        title: {
            marginLeft: theme.spacing(2),
            flex: 1
        },
        root: {
            display: 'flex',
            paddingLeft: drawerWidth
        },
        heading: {
            fontSize: theme.typography.pxToRem(15),
            flexBasis: '33.33%',
            flexShrink: 0
        },
        secondaryHeading: {
            fontSize: theme.typography.pxToRem(15),
            color: theme.palette.text.secondary
        },
        center: {
            padding: 20,
            margin: 'auto'
        },
        bg: {
            background: theme.palette.background.default
        },
        paper: {
            minWidth: '640px',
            margin: '20px'
        },
        drawer: {
            width: drawerWidth,
            marginRight: '20px',
            flexShrink: 0
        },
        drawerPaper: {
            width: drawerWidth,
            marginTop: '64px'
        },
        selected: {
            backgroundColor: theme.palette.background.default,
            color: theme.palette.primary.main
        }
    })
);

const Transition = React.forwardRef(
    (
        props: TransitionProps & { children?: React.ReactElement },
        ref: React.Ref<unknown>
    ) => <Slide direction="up" ref={ref} {...props} />
);

export default function FullScreenDialog() {
    const classes = useStyles();
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const showSettings = useSelector(
        (state: IState) => state.h5pPlayer.showSettings
    );
    const platformSupportsUpdates = useSelector(
        (state: IState) => state.system.platformSupportsUpdates
    );

    const section = useSelector(
        (state: IState) => state.h5pPlayer.settingsSection
    );

    console.log('section', section);

    const handleClose = () => {
        dispatch(actions.hideSettings());
    };

    return (
        <div>
            <Dialog
                fullScreen
                open={showSettings}
                onClose={handleClose}
                TransitionComponent={Transition}
            >
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            edge="start"
                            color="inherit"
                            onClick={handleClose}
                            aria-label="close"
                        >
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            {t('settings.appbar.label')}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Drawer
                    className={classes.drawer}
                    variant="persistent"
                    open
                    classes={{
                        paper: classes.drawerPaper
                    }}
                    anchor="left"
                >
                    <Divider />
                    <List>
                        <ListItem
                            button
                            key="general"
                            onClick={() =>
                                dispatch(actions.showGeneralSettings())
                            }
                            className={classnames({
                                [classes.selected]: section === 'general'
                            })}
                        >
                            <ListItemIcon>
                                <SettingsIcon />
                            </ListItemIcon>
                            <ListItemText
                                primary={t('settings.menu.general')}
                            />
                        </ListItem>
                        {platformSupportsUpdates && (
                            <ListItem
                                button
                                key="updates"
                                onClick={() =>
                                    dispatch(actions.showUpdatesSettings())
                                }
                                className={classnames({
                                    [classes.selected]: section === 'updates'
                                })}
                            >
                                <ListItemIcon>
                                    <UpdateIcon />
                                </ListItemIcon>
                                <ListItemText
                                    primary={t('settings.menu.updates')}
                                />
                            </ListItem>
                        )}
                    </List>
                </Drawer>
                <DialogContent className={classes.bg}>
                    <div className={classes.root}>
                        <div className={classes.center}>
                            {(() => {
                                switch (section) {
                                    case 'general':
                                    default:
                                        return (
                                            <div>
                                                <Paper
                                                    className={classes.paper}
                                                >
                                                    <LinkList />
                                                </Paper>
                                            </div>
                                        );

                                    case 'updates':
                                        return (
                                            <Paper className={classes.paper}>
                                                <UpdateSettings />{' '}
                                            </Paper>
                                        );
                                }
                            })()}
                        </div>
                    </div>
                </DialogContent>
            </Dialog>
        </div>
    );
}

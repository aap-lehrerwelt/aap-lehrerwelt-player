import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import MainSection from './components/MainSection';
import Root from './components/Root';
import { createStyles, Theme, withStyles } from '@material-ui/core/styles';
import H5PPlayerStartPage from './components/H5PPlayerStartPage';
import H5PPlayerComponentWrapper from './components/H5PPlayerComponentWrapper';
import LoadingPage from './components/LoadingPage';
import { actions, selectors } from '../state';
import { IContent, WidthLimits } from '../state/H5PPlayer/H5PPlayerTypes';
import ErrorScreen from './components/ErrorScreen';
import { IPlayerModel } from '@lumieducation/h5p-server';
import { Grid } from '@material-ui/core';
import PrivacyBlockedScreen from './components/PrivacyBlockedScreen';

export class H5PPlayer extends React.Component<{
    classes: any;
    content: IContent;
    error: {
        details: string;
        message: string;
    };
    isPrivacyBlocked: boolean;
    loadPlayerContent: (contentId: string) => Promise<IPlayerModel>;
    noActiveContent: boolean;
    openFiles: () => void;
    playerInitialized: () => void;
    viewDisabled: boolean;
    widthLimit: WidthLimits;
    zoomIn: () => void;
    zoomLevel: number;
    zoomOut: () => void;
}> {
    public render(): React.ReactNode {
        if (this.props.noActiveContent) {
            return (
                <Grid container justifyContent="center" direction="column">
                    <Grid item>
                        <H5PPlayerStartPage
                            primaryButtonClick={() => this.props.openFiles()}
                        />
                    </Grid>
                </Grid>
            );
        }
        return (
            <div id="h5pPlayer">
                <Root>
                    <MainSection>
                        {this.props.error !== undefined ? (
                            <ErrorScreen
                                message={this.props.error.message}
                                details={this.props.error.details}
                            />
                        ) : !this.props.content ||
                          this.props.content.opening ? (
                            <LoadingPage />
                        ) : this.props.isPrivacyBlocked ? (
                            <PrivacyBlockedScreen />
                        ) : (
                            <H5PPlayerComponentWrapper {...this.props} />
                        )}
                    </MainSection>
                </Root>
            </div>
        );
    }
}

function mapStateToProps(state: any, ownProps: any): any {
    return {
        content: selectors.h5pPlayer.activeContent(state),
        noActiveContent: selectors.h5pPlayer.noActiveContent(state),
        viewDisabled: selectors.h5pPlayer.viewDisabled(state),
        error: selectors.h5pPlayer.error(state),
        zoomLevel: selectors.h5pPlayer.zoomLevel(state),
        widthLimit: selectors.h5pPlayer.widthLimit(state),
        isPrivacyBlocked: selectors.h5pPlayer.isPrivacyBlocked(state)
    };
}

function mapDispatchToProps(dispatch: any): any {
    return bindActionCreators(
        {
            loadPlayerContent: actions.h5pPlayer.loadPlayerContent,
            playerInitialized: actions.h5pPlayer.playerInitialized,
            openFiles: actions.h5pPlayer.openFileDialog,
            zoomIn: actions.h5pPlayer.zoomIn,
            zoomOut: actions.h5pPlayer.zoomOut
        },
        dispatch
    );
}

const styles = (theme: Theme) => createStyles({});

export default withStyles(styles)(
    connect(mapStateToProps, mapDispatchToProps)(H5PPlayer)
);

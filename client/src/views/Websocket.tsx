import React from 'react';
import { connect } from 'react-redux';
import * as Sentry from '@sentry/browser';
import SocketIOClient from 'socket.io-client';
import { History } from 'history';
import Logger from '../helpers/Logger';
import { IContent } from '../state/H5PPlayer/H5PPlayerTypes';
import { actions, IState, selectors } from '../state';
import qs from 'querystring';

const log = new Logger('container:websocket');

interface IPassedProps {}

interface IStateProps extends IPassedProps {
    content: IContent;
    lockDisplay: boolean;
}

interface IDispatchProps {
    dispatch: (action: any) => void;
}

interface IComponentState {}

interface IProps extends IStateProps, IDispatchProps {}

declare var window: {
    h: History;
    location: any;
};

export class WebsocketContainer extends React.Component<
    IProps,
    IComponentState
> {
    constructor(props: IProps) {
        super(props);

        this.state = {};
        const { windowId } = qs.parse(window.location.search.replace('?', ''));

        this.socket = SocketIOClient(`/${windowId}`);
    }

    private socket: SocketIOClient.Socket;

    public componentDidMount(): void {
        const { dispatch } = this.props;

        this.socket.on('connect', () => {
            log.info('connected');
        });

        this.socket.on('error', (error: any) => {
            Sentry.captureException(error);
        });

        this.socket.on('action', (action: any) => {
            if (!this.props.lockDisplay) {
                switch (action.type) {
                    case 'action':
                        this.props.dispatch({
                            payload: action.payload.payload,
                            type: action.payload.type
                        });
                        break;

                    case 'OPEN_H5P':
                        action.payload.paths.forEach((file: any) => {
                            dispatch(
                                actions.h5pPlayer.importH5P(file, window.h)
                            );
                        });
                        break;

                    case 'REPORT_ISSUE':
                        Sentry.showReportDialog();
                        break;

                    case 'MESSAGE':
                        dispatch(
                            actions.notifications.notify(
                                action.payload.message,
                                action.payload.type
                            )
                        );
                }
            }
        });
    }

    public render(): null {
        log.info(`rendering`);
        return null;
    }
}

function mapStateToProps(state: IState, ownProps: IPassedProps): IStateProps {
    return {
        content: selectors.h5pPlayer.activeContent(state),
        lockDisplay: state.h5pPlayer.lockDisplay
    };
}

function mapDispatchToProps(dispatch: any): IDispatchProps {
    return { dispatch: (action: any) => dispatch(action) };
}

export default connect<IStateProps, IDispatchProps, IPassedProps, IState>(
    mapStateToProps,
    mapDispatchToProps
)(WebsocketContainer);

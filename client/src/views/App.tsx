import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import CssBaseline from '@material-ui/core/CssBaseline';

import Logger from '../helpers/Logger';
import AppBar from './components/AppBar';
import Notifications from './Notifications';
import H5PPlayerView from './H5PPlayer';
import SetupDialog from './components/SetupDialog';
import Backdrop from './components/Backdrop';
import Websocket from './Websocket';
import ErrorDialog from './components/ErrorDialog';
import Settings from './Settings';

import { actions } from '../state';

const log = new Logger('container:app');

export default function AppContainer(): JSX.Element {
    log.info(`rendering`);
    const dispatch = useDispatch();
    const { i18n } = useTranslation();

    useEffect(() => {
        dispatch(actions.settings.getSettings()).then(
            async (settings: { autoUpdates: boolean; language: string }) => {
                if (settings?.language) {
                    await i18n.loadLanguages(settings.language);
                    i18n.changeLanguage(settings.language);
                }
                if (settings?.autoUpdates) {
                    dispatch(actions.updates.getUpdates());
                }
            }
        );

        dispatch(actions.system.getSystem());
    }, [dispatch, i18n]);

    return (
        <div id="app">
            <CssBaseline />
            <Router>
                <Websocket />
                <AppBar />
                <Switch>
                    <Route path="/" component={H5PPlayerView} />
                </Switch>
                <SetupDialog />
                <Settings />
                <ErrorDialog />
            </Router>
            <Notifications />
            <Backdrop />
        </div>
    );
}

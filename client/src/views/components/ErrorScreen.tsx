import React from 'react';
import { useTranslation } from 'react-i18next';

import ErrorIcon from '@material-ui/icons/Error';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            display: 'flex',
            height: '100%',
            paddingLeft: '2em',
            paddingRight: '2em'
        }
    })
);

export default function ErrorScreen(props: {
    details: string;
    message: string;
}): JSX.Element {
    const classes = useStyles();
    const { t } = useTranslation();

    return (
        <Box
            className={classes.root}
            display="flex"
            flexDirection="column"
            justifyContent="center"
        >
            <h1 style={{ textAlign: 'center' }}>
                <ErrorIcon color="error" /> {t('errorScreen.title')}
            </h1>
            <h2 style={{ textAlign: 'center' }}>{props.message}</h2>
            <h3 style={{ textAlign: 'center' }}>{props.details}</h3>
        </Box>
    );
}

import React from 'react';
import { createStyles, Theme, withStyles } from '@material-ui/core/styles';
import { H5PPlayerUI } from '@lumieducation/h5p-react';
import { IPlayerModel } from '@lumieducation/h5p-server';

import LoadingProgressBar from './LoadingProgressBar';
import { IContent, WidthLimits } from '../../state/H5PPlayer/H5PPlayerTypes';
import i18n from '../../boot/i18n';
import { getSystemInfo } from '../../services/systemInfo';
import { ScaleBox } from './ScaleBox';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid,
    Tooltip
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import TitleToolbar from './TitleToolbar';

interface IH5PPlayerComponentWrapperProps {
    classes: any;
    content: IContent;
    loadPlayerContent: (contentId: string) => Promise<IPlayerModel>;
    playerInitialized: () => void;
    widthLimit: WidthLimits;
    zoomIn: () => void;
    zoomLevel: number;
    zoomOut: () => void;
}

interface IH5PPlayerState {
    copyrightHtml: string;
    hasCopyright: boolean;
    showingCopyright: boolean;
}

export class H5PPlayerComponentWrapper extends React.Component<
    IH5PPlayerComponentWrapperProps,
    IH5PPlayerState
> {
    constructor(props: IH5PPlayerComponentWrapperProps) {
        super(props);
        this.h5pPlayer = React.createRef();
        this.state = {
            showingCopyright: false,
            hasCopyright: false,
            copyrightHtml: i18n.t('lumi:copyright.notAvailable')
        };
    }

    private h5pPlayer: React.RefObject<H5PPlayerUI>;

    public componentDidUpdate(
        prevProps: IH5PPlayerComponentWrapperProps
    ): void {
        if (this.props.zoomLevel !== prevProps.zoomLevel) {
            this.h5pPlayer.current?.resize();
        }
    }

    public componentWillUnmount(): void {
        document.title = i18n.t('lumi:title');
    }

    public render(): React.ReactNode {
        return (
            <div style={{ width: '100%', paddingBottom: '20px' }}>
                <Dialog
                    open={this.state.showingCopyright}
                    onClose={this.closeCopyrightDialog}
                >
                    <DialogTitle>{i18n.t('lumi:copyright.title')}</DialogTitle>
                    <DialogContent>
                        <div
                            dangerouslySetInnerHTML={{
                                __html: this.state.copyrightHtml
                            }}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button
                            autoFocus
                            color="primary"
                            onClick={this.closeCopyrightDialog}
                        >
                            {i18n.t('lumi:dialog.close')}
                        </Button>
                    </DialogActions>
                </Dialog>
                {this.props.content.loadingIndicator ? (
                    <LoadingProgressBar />
                ) : null}
                <TitleToolbar
                    title={this.props.content.name}
                    zoomLevel={this.props.zoomLevel}
                    zoomIn={this.props.zoomIn}
                    zoomOut={this.props.zoomOut}
                />
                <ScaleBox
                    scale={this.props.zoomLevel}
                    style={{
                        marginTop: '45px',
                        maxWidth: widthLimitToPx(this.props.widthLimit)
                    }}
                >
                    <Paper
                        style={{
                            padding: '20px',
                            marginLeft: '20px',
                            marginRight: '20px',
                            marginBottom: '20px'
                        }}
                    >
                        {this.props.content.contentId ? (
                            <H5PPlayerUI
                                ref={this.h5pPlayer}
                                contentId={this.props.content.contentId}
                                loadContentCallback={this.loadPlayerContent}
                                onInitialized={this.playerInitialized}
                            />
                        ) : null}
                    </Paper>
                    {this.state.hasCopyright && (
                        <Tooltip
                            title={i18n.t('lumi:copyright.title') as string}
                        >
                            <Grid
                                container
                                direction="column"
                                alignItems="flex-end"
                            >
                                <Grid item>
                                    <Button
                                        variant="contained"
                                        size="small"
                                        style={{
                                            color: '#606060',
                                            marginRight: '20px'
                                        }}
                                        onClick={() => {
                                            this.setState({
                                                ...this.state,
                                                showingCopyright: true
                                            });
                                        }}
                                    >
                                        {i18n.t('lumi:copyright:title')}
                                    </Button>
                                </Grid>
                            </Grid>
                        </Tooltip>
                    )}
                </ScaleBox>
            </div>
        );
    }

    private loadPlayerContent = async (
        contentId: string
    ): Promise<IPlayerModel> => {
        const playerModel = await this.props.loadPlayerContent(contentId);
        if (playerModel.integration.contents) {
            const file =
                playerModel.integration.contents[`cid-${playerModel.contentId}`]
                    ?.metadata?.title;
            const platform = (await getSystemInfo()).platform;
            if (platform === 'mac' || platform === 'mas') {
                document.title = file ?? i18n.t('lumi:title');
            } else {
                document.title = file
                    ? i18n.t('lumi:titleWithFile', { file })
                    : i18n.t('lumi:title');
            }
        }
        return playerModel;
    };

    private playerInitialized = () => {
        let hasCopyright: boolean = false;
        try {
            hasCopyright =
                this.h5pPlayer.current?.hasCopyrightInformation() ?? false;
        } catch {}
        let copyrightHtml: string;
        try {
            copyrightHtml =
                this.h5pPlayer.current?.getCopyrightHtml() ??
                i18n.t('lumi:copyright.notAvailable');
        } catch {
            copyrightHtml = '';
        }

        this.setState({
            ...this.state,
            hasCopyright,
            copyrightHtml
        });
        this.props.playerInitialized();
    };

    private closeCopyrightDialog = () => {
        this.setState({
            ...this.state,
            showingCopyright: false
        });
    };
}

const widthLimitToPx = (
    widthLimit: WidthLimits,
    scale?: number
): string | undefined => {
    let pixels: number = 0;
    switch (widthLimit) {
        case WidthLimits.Narrow:
            pixels = 800;
            break;
        case WidthLimits.Medium:
            pixels = 1100;
            break;
        case WidthLimits.Wide:
            pixels = 1400;
            break;
        case WidthLimits.ExtraWide: // currently not usable in the UI
            pixels = 1800;
            break;
        default:
            break;
    }
    if (!pixels) {
        return undefined;
    }
    return `${pixels * (scale ?? 1)}px`;
};

const styles = (theme: Theme) =>
    createStyles({
        modeTab: {
            backgroundColor: theme.palette.background.paper,
            display: 'fixed',
            flexGrow: 1
        }
    });

export default withStyles(styles)(H5PPlayerComponentWrapper);

import * as React from 'react';
import useResizeObserver from '@react-hook/resize-observer';

interface Props
    extends React.DetailedHTMLProps<
        React.HTMLAttributes<HTMLDivElement>,
        HTMLDivElement
    > {
    scale?: number;
    style?: React.CSSProperties;
}

/**
 * The ScaleBox is an element that scales its content using CSS transform scale
 * and makes sure the flow around the box is as if the box had the size
 * according to the applied scale.
 *
 * It also centers the content horizontally taking the scale into consideration.
 */
export const ScaleBox: React.FC<Props> = ({ scale = 1, style, children }) => {
    const [marginLeft, setLeftMargin] = React.useState('0px');
    const [marginRight, setRightMargin] = React.useState('0px');
    const [marginBottom, setBottomMargin] = React.useState('0px');
    const outerDivRef = React.useRef<HTMLDivElement>(null);
    const innerDivRef = React.useRef<HTMLDivElement>(null);

    useResizeObserver(outerDivRef, (target) => {
        const child = target.target.firstElementChild;
        let mLeft = 0;
        if (child && target.contentRect.width > child.clientWidth * scale) {
            mLeft = (target.contentRect.width - child.clientWidth * scale) / 2;
        }
        setLeftMargin(`${mLeft}px`);
    });

    useResizeObserver(innerDivRef, (target) => {
        const parent = target.target.parentElement;
        let offsetX = 0;
        if (parent && parent.clientWidth > target.contentRect.width * scale) {
            offsetX =
                (parent.clientWidth - target.contentRect.width * scale) / 2;
        }

        let mRight = (scale - 1) * target.contentRect.width;
        if (mRight > 0) {
            mRight = 0;
        }
        let mBottom = (scale - 1) * target.contentRect.height;

        setLeftMargin(`${offsetX}px`);
        setRightMargin(`${mRight}px`);
        setBottomMargin(`${mBottom}px`);
    });

    React.useEffect(() => {
        if (innerDivRef.current) {
            let mLeft = 0;
            if (outerDivRef.current) {
                if (
                    outerDivRef.current.offsetWidth >
                    innerDivRef.current.offsetWidth * scale
                ) {
                    mLeft =
                        (outerDivRef.current.offsetWidth -
                            innerDivRef.current.offsetWidth * scale) /
                        2;
                }
            }
            let mRight = (scale - 1) * innerDivRef.current.offsetWidth;
            if (mRight > 0) {
                mRight = 0;
            }
            let mBottom = (scale - 1) * innerDivRef.current.offsetHeight;

            setLeftMargin(`${mLeft}px`);
            setRightMargin(`${mRight}px`);
            setBottomMargin(`${mBottom}px`);
        }
    }, [scale]);

    return (
        <div
            ref={outerDivRef}
            style={{
                overflow: scale < 1 ? 'hidden' : undefined
            }}
        >
            <div
                ref={innerDivRef}
                style={{
                    ...style,
                    transform: `scale(${scale})`,
                    transformOrigin: 'top left',
                    marginLeft: marginLeft,
                    marginRight: marginRight,
                    marginBottom: marginBottom
                }}
            >
                {children}
            </div>
        </div>
    );
};

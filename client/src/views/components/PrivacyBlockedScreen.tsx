import { useTranslation } from 'react-i18next';

import SecurityIcon from '@material-ui/icons/Security';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Box } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            display: 'flex',
            height: '100%',
            paddingLeft: '2em',
            paddingRight: '2em'
        }
    })
);

export default function PrivacyBlockedScreen(): JSX.Element {
    const classes = useStyles();
    const { t } = useTranslation();

    return (
        <Box
            className={classes.root}
            display="flex"
            flexDirection="column"
            justifyContent="center"
        >
            <h2 style={{ textAlign: 'center' }}>
                <SecurityIcon style={{ verticalAlign: 'middle' }} />{' '}
                <span style={{ verticalAlign: 'middle' }}>
                    {t('lumi:privacyBlockedScreen.title')}
                </span>
            </h2>
            <h3 style={{ textAlign: 'center' }}>
                {t('lumi:privacyBlockedScreen.explanation')}
            </h3>
        </Box>
    );
}

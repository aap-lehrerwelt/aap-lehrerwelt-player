import * as React from 'react';
import { useTranslation } from 'react-i18next';

import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { makeStyles, Theme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import InsertDriveFileOutlinedIcon from '@material-ui/icons/InsertDriveFileOutlined';
import { Box, Paper } from '@material-ui/core';

export default function H5PPlayerStartPage(props: {
    primaryButtonClick: () => void;
}): JSX.Element {
    const classes = useStyles();
    const { t } = useTranslation();
    return (
        <Paper style={{ margin: '20px', padding: '20px' }}>
            <Box>
                <Container maxWidth="sm" className={classes.heroContent}>
                    <Typography
                        variant="h5"
                        align="center"
                        color="textSecondary"
                        paragraph
                    >
                        {t('player.startPage.welcomeMessage')}
                    </Typography>
                    <div className={classes.heroButtons}>
                        <Grid container spacing={2} justifyContent="center">
                            <Grid item>
                                <Button
                                    id="editor-startpage-primaryButton"
                                    onClick={() => {
                                        props.primaryButtonClick();
                                    }}
                                    variant="contained"
                                    color="primary"
                                    startIcon={<InsertDriveFileOutlinedIcon />}
                                >
                                    {t('player.startPage.open')}
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                </Container>
            </Box>
        </Paper>
    );
}

const useStyles = makeStyles((theme: Theme) => {
    return {
        card: {
            display: 'flex',
            flexDirection: 'column',
            height: '100%'
        },
        cardContent: {
            flexGrow: 1
        },
        cardGrid: {
            paddingBottom: theme.spacing(8),
            paddingTop: theme.spacing(8)
        },
        cardMedia: {
            paddingTop: '56.25%' // 16:9
        },
        footer: {
            backgroundColor: theme.palette.background.paper,
            padding: theme.spacing(6)
        },
        heroButtons: {
            marginTop: theme.spacing(4)
        },
        heroContent: {
            backgroundColor: theme.palette.background.paper,
            padding: theme.spacing(8, 0, 6)
        },
        icon: {
            marginRight: theme.spacing(2)
        }
    };
});

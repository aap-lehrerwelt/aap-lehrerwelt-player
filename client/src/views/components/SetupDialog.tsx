import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import {
    createStyles,
    Theme,
    withStyles,
    makeStyles,
    WithStyles
} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Switch from '@material-ui/core/Switch';
import UpdateIcon from '@material-ui/icons/Update';
import { Description } from '@material-ui/icons';

import { showPrivacyPolicy } from '../../services/systemInfo';
import LinkList from './Settings/LinkList';
import { IState, actions } from '../../state';

const styles = (theme: Theme) =>
    createStyles({
        root: {
            margin: 0,
            width: '400px',
            padding: theme.spacing(2)
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(1),
            top: theme.spacing(1),
            color: theme.palette.grey[500]
        },
        list: {
            width: '100%',
            backgroundColor: theme.palette.background.paper
        }
    });

const useStyles = makeStyles((theme: Theme) => {
    return {
        warning: {
            color: 'red'
        },
        button: {
            margin: theme.spacing(1)
        }
    };
});

export interface DialogTitleProps extends WithStyles<typeof styles> {
    id: string;
    children: React.ReactNode;
    onClose: () => void;
}

const DialogContent = withStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2)
    }
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
        minWidth: 380
    }
}))(MuiDialogActions);

export default function CustomizedDialogs() {
    const dispatch = useDispatch();
    const settings = useSelector((state: IState) => state.settings);
    const [hasLicenseConsented, setLicenseConsent] = useState(
        settings.licenseConsent
    );
    const [hasEnabledUpdates, setHasEnabledUpdated] = useState(
        settings.autoUpdates
    );

    const platformSupportsUpdates = useSelector(
        (state: IState) => state.system.platformSupportsUpdates
    );

    const classes = useStyles();
    const { t } = useTranslation();

    const handleSave = () => {
        dispatch(
            actions.settings.updateSettings({
                ...settings,
                autoUpdates: hasEnabledUpdates,
                licenseConsent: hasLicenseConsented,
                firstOpen: false
            })
        );
    };

    const handleAcceptAll = () => {
        dispatch(
            actions.settings.updateSettings({
                ...settings,
                licenseConsent: true,
                autoUpdates: true,
                firstOpen: false
            })
        );
    };

    return (
        <Dialog
            aria-labelledby="customized-dialog-title"
            open={
                settings.areLoaded &&
                (settings.firstOpen || !settings.licenseConsent)
            }
        >
            <DialogContent dividers>
                <Typography variant="body2" gutterBottom>
                    {t('setup_dialog.description')}
                </Typography>
                <LinkList />

                <List
                    subheader={
                        <ListSubheader disableSticky>
                            {t('settings.appbar.label')}
                        </ListSubheader>
                    }
                >
                    <ListItem>
                        <ListItemIcon>
                            <Description />
                        </ListItemIcon>
                        <ListItemText
                            id="switch-list-label-license"
                            primary={t('setup_dialog.license.title')}
                            secondary={t('setup_dialog.license.consent')}
                            style={{ marginRight: '30px' }}
                        />
                        <ListItemSecondaryAction>
                            <Switch
                                edge="end"
                                onChange={() =>
                                    setLicenseConsent(!hasLicenseConsented)
                                }
                                checked={hasLicenseConsented}
                                inputProps={{
                                    'aria-labelledby':
                                        'switch-list-label-license'
                                }}
                            />
                        </ListItemSecondaryAction>
                    </ListItem>
                    {platformSupportsUpdates && (
                        <ListItem>
                            <ListItemIcon>
                                <UpdateIcon />
                            </ListItemIcon>
                            <ListItemText
                                id="switch-list-label-updates"
                                primary={t('updates.title')}
                                secondary={
                                    <span>
                                        {t('updates.consent')}&nbsp;
                                        <button
                                            type="button"
                                            style={{
                                                backgroundColor: 'transparent',
                                                border: 'none',
                                                cursor: 'pointer',
                                                textDecoration: 'underline',
                                                display: 'inline',
                                                margin: 0,
                                                padding: 0
                                            }}
                                            onClick={() => showPrivacyPolicy()}
                                        >
                                            {t('updates.moreInfo')}
                                        </button>
                                    </span>
                                }
                                style={{ marginRight: '30px' }}
                            />
                            <ListItemSecondaryAction>
                                <Switch
                                    edge="end"
                                    onChange={() =>
                                        setHasEnabledUpdated(!hasEnabledUpdates)
                                    }
                                    checked={hasEnabledUpdates}
                                    inputProps={{
                                        'aria-labelledby':
                                            'switch-list-label-updates'
                                    }}
                                />
                            </ListItemSecondaryAction>
                        </ListItem>
                    )}
                </List>
                {hasLicenseConsented ? null : (
                    <Typography
                        className={classes.warning}
                        variant="body2"
                        gutterBottom
                    >
                        {t('setup_dialog.consent_warning')}
                    </Typography>
                )}
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={handleSave}
                    color="secondary"
                    disabled={!hasLicenseConsented}
                >
                    {t('setup_dialog.accept_selected')}
                </Button>
                <Button autoFocus onClick={handleAcceptAll} color="primary">
                    {t('setup_dialog.accept_all')}
                </Button>
            </DialogActions>
        </Dialog>
    );
}

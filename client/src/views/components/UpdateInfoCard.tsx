import React from 'react';
import { useTranslation } from 'react-i18next';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import { CardHeader } from '@material-ui/core';
import { ISystemInformation } from '../../services/systemInfo';

export default function UpdateInfoCard(props: {
    releaseDate: string;
    releaseName: string;
    releaseNotes: string;
    releaseVersion: string;
    systemInfo: ISystemInformation;
    updateCallback: () => Promise<void>;
}) {
    const { t } = useTranslation();
    const { releaseVersion, updateCallback } = props;
    const [updating, setUpdating] = React.useState<boolean>(false);

    const update = async () => {
        setUpdating(true);
        await updateCallback();
        setUpdating(false);
    };

    return (
        <Card style={{ margin: '10px' }}>
            <CardHeader title={t('updates.updateAvailableHeader')} />
            <CardContent>
                {t(
                    props.systemInfo?.platform === 'mac'
                        ? 'updates.updateTextMac'
                        : 'updates.updateText',
                    {
                        version: releaseVersion
                    }
                )}
            </CardContent>
            {props.systemInfo?.platform !== 'mac' && (
                <CardActions>
                    <Button
                        variant="outlined"
                        size="small"
                        color="primary"
                        disabled={updating}
                        onClick={update}
                        style={{ marginBottom: '10px' }}
                    >
                        {updating
                            ? t('updates.loadingUpdateAndRestarting')
                            : t('updates.restartAndInstall')}
                    </Button>
                </CardActions>
            )}
        </Card>
    );
}

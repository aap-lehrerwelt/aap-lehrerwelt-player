import React from 'react';

import { default as MAppBar } from '@material-ui/core/AppBar';
import { makeStyles, Theme } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import i18n from '../../boot/i18n';
import { Button } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import * as actions from '../../state/H5PPlayer/H5PPlayerActions';
import { IState } from '../../state';

export default function AppBar(props: {}): JSX.Element {
    const classes = useStyles();
    const updateInfo = useSelector((state: IState) => state.updates.updateInfo);
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const handleClickOpen = () => {
        dispatch(actions.showUpdatesSettings());
    };

    return (
        <MAppBar position="fixed" className={classes.appBar}>
            <Toolbar variant="dense" className={classes.toolbar}>
                <Typography variant="h6" noWrap>
                    {i18n.t('lumi:title')}
                </Typography>
                {updateInfo?.version && (
                    <Button
                        style={{ marginLeft: '20px', color: 'white' }}
                        variant="outlined"
                        size="small"
                        onClick={handleClickOpen}
                    >
                        {t('updates.badge')}
                    </Button>
                )}
                <div className={classes.grow} />
            </Toolbar>
        </MAppBar>
    );
}

const useStyles = makeStyles((theme: Theme) => {
    return {
        appBar: {
            zIndex: theme.zIndex.drawer + 1
        },
        grow: {
            flexGrow: 1
        },
        toolbar: {
            paddingRight: '0px'
        },

        greenBg: {
            background: '#33AA82',
            height: '100%',
            paddingRight: '24px',
            paddingLeft: '24px',
            paddingTop: '13px',
            paddingBottom: '13px',
            borderLeft: '6px solid #A0CC8b'
        },
        hide: {
            display: 'none'
        },
        menuButton: {
            marginRight: theme.spacing(2)
        }
    };
});

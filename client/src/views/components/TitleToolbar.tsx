import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import i18n from '../../boot/i18n';
import { IconButton, Tooltip } from '@material-ui/core';

import { ZoomIn, ZoomOut } from '@material-ui/icons';
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
        menuButton: {
            marginRight: theme.spacing(2)
        },
        title: {
            flexGrow: 1,
            color: 'black'
        }
    })
);

export default function TitleToolbar(props: {
    title: string;
    zoomLevel: number;
    zoomIn: () => void;
    zoomOut: () => void;
}) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar
                position="fixed"
                style={{
                    background: '#f1f3f4',
                    border: 'none',
                    marginTop: '48px'
                }}
            >
                <Toolbar variant="dense">
                    <Typography
                        variant="h6"
                        component="div"
                        style={{
                            textOverflow: 'ellipsis',
                            overflow: 'hidden',
                            flexGrow: 1,
                            fontSize: `${1.2 * props.zoomLevel}rem`,
                            color: 'black'
                        }}
                    >
                        {props.title}
                    </Typography>
                    <Tooltip title={i18n.t('lumi:menu.view.zoomOut') as string}>
                        <IconButton onClick={props.zoomOut} color="secondary">
                            <ZoomOut />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title={i18n.t('lumi:menu.view.zoomIn') as string}>
                        <IconButton onClick={props.zoomIn} color="secondary">
                            <ZoomIn />
                        </IconButton>
                    </Tooltip>
                </Toolbar>
            </AppBar>
        </div>
    );
}

import React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';
import { Grid } from '@material-ui/core';

export default function LoadingPage() {
    return (
        <Grid
            container
            alignContent="center"
            justifyContent="center"
            style={{ height: '100%' }}
        >
            <Grid item>
                <CircularProgress size={100} />
            </Grid>
        </Grid>
    );
}

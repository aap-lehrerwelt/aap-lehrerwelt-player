import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Switch from '@material-ui/core/Switch';
import Button from '@material-ui/core/Button';
import UpdateIcon from '@material-ui/icons/Update';

import UpdateInfoCard from '../UpdateInfoCard';
import { actions, IState } from '../../../state';
import { showPrivacyPolicy } from '../../../services/systemInfo';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            backgroundColor: theme.palette.background.paper
        }
    })
);

export default function UpdatesSettingsList() {
    const classes = useStyles();

    const dispatch = useDispatch();
    const { t } = useTranslation();
    const settings = useSelector((state: IState) => state.settings);
    const updateInfo = useSelector((state: IState) => state.updates.updateInfo);
    const hasChecked = useSelector((state: IState) => state.updates.hasChecked);
    const systemInfo = useSelector((state: IState) => state.system);

    return (
        <div>
            <List
                subheader={
                    <ListSubheader disableSticky>
                        {t('settings.updates.header')}
                    </ListSubheader>
                }
                className={classes.root}
            >
                {!updateInfo?.version && (
                    <ListItem>
                        <ListItemIcon>
                            <UpdateIcon />
                        </ListItemIcon>
                        <ListItemText
                            id="switch-list-label-updates"
                            primary={
                                <Button
                                    variant="outlined"
                                    disabled={!settings.autoUpdates}
                                    onClick={() =>
                                        dispatch(actions.updates.getUpdates())
                                    }
                                >
                                    {t('updates.checkForUpdate.checkButton')}
                                </Button>
                            }
                            secondary={
                                hasChecked && (
                                    <div style={{ marginTop: '10px' }}>
                                        {t('updates.noUpdateText')}
                                    </div>
                                )
                            }
                        />
                    </ListItem>
                )}
                {settings.autoUpdates && updateInfo?.version && (
                    <UpdateInfoCard
                        updateCallback={async () =>
                            dispatch(actions.updates.update())
                        }
                        releaseName={updateInfo.releaseName}
                        releaseNotes={updateInfo.releaseNotes}
                        releaseDate={updateInfo.releaseDate}
                        releaseVersion={updateInfo.version}
                        systemInfo={systemInfo}
                    />
                )}
                <ListItem>
                    <ListItemIcon>
                        <UpdateIcon />
                    </ListItemIcon>
                    <ListItemText
                        id="switch-list-label-updates"
                        primary={t('updates.title')}
                        secondary={
                            <span>
                                {t('updates.consent')}&nbsp;
                                <button
                                    type="button"
                                    style={{
                                        backgroundColor: 'transparent',
                                        border: 'none',
                                        cursor: 'pointer',
                                        textDecoration: 'underline',
                                        display: 'inline',
                                        margin: 0,
                                        padding: 0
                                    }}
                                    onClick={() => showPrivacyPolicy()}
                                >
                                    {t('updates.moreInfo')}
                                </button>
                            </span>
                        }
                    />
                    <ListItemSecondaryAction>
                        <Switch
                            edge="end"
                            onChange={() =>
                                dispatch(
                                    actions.settings.changeSetting({
                                        autoUpdates: !settings.autoUpdates
                                    })
                                )
                            }
                            checked={settings.autoUpdates}
                            inputProps={{
                                'aria-labelledby': 'switch-list-label-updates'
                            }}
                        />
                    </ListItemSecondaryAction>
                </ListItem>
            </List>
        </div>
    );
}

import React from 'react';
import { useTranslation } from 'react-i18next';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import PolicyIcon from '@material-ui/icons/Policy';
import { Description } from '@material-ui/icons';
import { showLicense, showPrivacyPolicy } from '../../../services/systemInfo';

export default function SettingsLinkList() {
    const { t } = useTranslation();

    return (
        <List
            subheader={
                <ListSubheader disableSticky>
                    {t('settings.links.header')}
                </ListSubheader>
            }
        >
            <ListItem
                button
                onClick={async () => {
                    await showLicense();
                }}
            >
                <ListItemIcon>
                    <Description />
                </ListItemIcon>
                <ListItemText
                    id="switch-list-label-license"
                    primary={t('setup_dialog.license.title')}
                />
            </ListItem>
            <ListItem
                button
                onClick={async () => {
                    await showPrivacyPolicy();
                }}
            >
                <ListItemIcon>
                    <PolicyIcon />
                </ListItemIcon>
                <ListItemText
                    id="switch-list-label-privacy-policy"
                    primary={t('privacy_policy.title')}
                />
            </ListItem>
        </List>
    );
}

# Views

## Main modules

-   `Notifications.ts` - Displays notifications
-   `App.tsx` - Entry poiny and main router. This file includes the AppBar and
    Notifications-Module, present on all pages.
-   `components` - reusable components used across different pages/views.

import superagent from 'superagent';

export interface ISystemInformation {
    platform: Platform;
    platformSupportsUpdates: boolean;
}

type Platform = 'mac' | 'mas' | 'win' | 'win-store' | 'linux' | NodeJS.Platform;

let cache: ISystemInformation;

export async function getSystemInfo(): Promise<ISystemInformation> {
    if (!cache) {
        cache = (await superagent.get(`/api/v1/system`)).body;
    }
    return cache;
}

export async function showLicense(): Promise<void> {
    await superagent.post('/api/v1/lumi/showLicense').send({});
}

export async function showPrivacyPolicy(): Promise<void> {
    await superagent.post('/api/v1/lumi/showPrivacyPolicy').send({});
}

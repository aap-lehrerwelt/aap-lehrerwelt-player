import { createTheme } from '@material-ui/core/styles';

export const drawerWidth = 240;

// A custom theme for this app
const theme = createTheme({
    palette: {
        error: {
            main: '#c0392b'
        },
        primary: {
            main: '#3498db'
        },
        secondary: {
            main: '#1abc9c'
        },
        background: {
            default: '#f1f3f4'
        }
    },
    props: {
        MuiAppBar: {
            style: {
                background: '#1b7fb6'
            }
        }
    }
});

export default theme;

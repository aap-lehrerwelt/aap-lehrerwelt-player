import { BrowserWindow, dialog } from 'electron';
import fs from 'fs-extra';
import _path from 'path';
import * as H5P from '@lumieducation/h5p-server';

import Logger from '../helpers/Logger';
import User from '../h5pImplementations/User';
import WindowManager from '../state/WindowManager';

const log = new Logger('controller:lumi-h5p');

export default class LumiController {
    constructor(
        private h5pEditor: H5P.H5PEditor,
        private getBrowserWindow: () => BrowserWindow,
        private getWindowManager: () => WindowManager
    ) {}

    /**
     * Loads a .h5p file from the disk and imports it into the H5P server.
     * @param path
     * @returns
     */
    public async importH5pFile(
        path: string,
        windowId: string,
        activeContentId: string | undefined
    ): Promise<{
        id: string;
        library: string;
        metadata: H5P.IContentMetadata;
        parameters: any;
    }> {
        if (activeContentId) {
            try {
                this.h5pEditor.deleteContent(activeContentId, new User());
            } catch {
                log.error(
                    `Error while deleting temporary files for opened content with id ${activeContentId}`
                );
            }
        }

        const buffer = await fs.readFile(path);

        const { metadata, parameters } = await this.h5pEditor.uploadPackage(
            buffer,
            new User()
        );

        const id = await this.h5pEditor.saveOrUpdateContent(
            undefined,
            parameters,
            metadata,
            this.getUbernameFromH5pJson(metadata),
            new User()
        );

        await this.getWindowManager()
            ?.getWindow(windowId)
            ?.setContent(path, id);

        return {
            id,
            metadata,
            parameters,
            library: this.getUbernameFromH5pJson(metadata)
        };
    }

    /**
     * Opens a file picker that allows the users to chose H5P files.
     * @returns
     */
    public async selectFiles(): Promise<string[]> {
        const response = await dialog.showOpenDialog(this.getBrowserWindow(), {
            filters: [
                {
                    extensions: ['h5p'],
                    name: 'HTML 5 Package'
                }
            ],
            properties: ['openFile', 'multiSelections']
        });

        return response.filePaths;
    }

    private getUbernameFromH5pJson(h5pJson: H5P.IContentMetadata): string {
        const library = (h5pJson.preloadedDependencies || []).find(
            (dependency) => dependency.machineName === h5pJson.mainLibrary
        );
        if (!library) {
            return '';
        }
        return H5P.LibraryName.toUberName(library, { useWhitespace: true });
    }
}

/**
 * This file contains the Electron app initialization and is the main entry
 * point into the whole app. It initializes all other components and parses
 * command line arguments and other events received from the OS.
 */

import electron from 'electron';
import log from 'electron-log';
import path from 'path';
import SocketIO from 'socket.io';
import fsExtra from 'fs-extra';

import createHttpServer from './boot/httpServer';
import initUpdater from './boot/updater';
import createWebsocket from './boot/websocket';
import serverPathGenerator from './config/defaultPaths';
import loadSettings from './boot/loadSettings';
import WindowManager from './state/WindowManager';
import updateMenu from './menu';
import initH5P from './boot/h5p';
import createExpressApp from './boot/expressApp';
import User from './h5pImplementations/User';

const app = electron.app;
let websocket: SocketIO.Server;

/**
 * The port used by the internal NodeJS server.
 */
let port: number;
/**
 * Whether we run in development mode.
 */
const isDevelopment = process.env.NODE_ENV === 'development';

/**
 * windowManger to manage all open windows.
 */
const windowManager = new WindowManager(
    isDevelopment,
    async () => {
        updateMenu('/', windowManager);
    },
    async () => {
        updateMenu('/', windowManager);
    }
);

const tmpDir = path.join(app.getPath('temp'), app.name);
const serverPaths = serverPathGenerator(
    process.env.USERDATA || app.getPath('userData'),
    tmpDir
);

process.on('uncaughtException', (error) => {
    log.error(error);
});

// By requesting the single instance lock we make sure there's only one process
// of the app.
const gotSingleInstanceLock = app.requestSingleInstanceLock();
if (!gotSingleInstanceLock) {
    app.quit();
} else {
    // quit application when all windows are closed
    app.on('window-all-closed', () => {
        // on macOS it is common for applications to stay open until the user explicitly quits
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });

    app.on('second-instance', (event, argv) => {
        if (argv.length >= 2) {
            // Check if there are H5Ps specified in the command line args and
            // load them (Windows only).
            argv.splice(0, 1);
            const openFilePaths = argv.filter((arg) => arg.endsWith('.h5p'));
            if (openFilePaths.length > 0) {
                log.debug(`Opening file(s): ${openFilePaths.join(' ')}`);
                for (const file of openFilePaths) {
                    windowManager.openH5P(file, false);
                }
            } else {
                windowManager.restoreOrNewWindow();
            }
        } else {
            windowManager.restoreOrNewWindow();
        }
    });

    // Handle open file events for MacOS
    app.on('open-file', (event: electron.Event, openedFilePath: string) => {
        log.debug('Electron open-file event caught');
        windowManager.openH5P(openedFilePath, false);

        event.preventDefault();
    });

    app.on('activate', () => {
        // on macOS it is common to re-create a window even after all windows have been closed
        windowManager.restoreOrNewWindow();
    });

    // create main BrowserWindow when electron is ready
    app.on('ready', async () => {
        // Make sure required directories exist
        await fsExtra.mkdirp(serverPaths.contentPath);
        await fsExtra.mkdirp(serverPaths.librariesPath);
        await fsExtra.mkdirp(serverPaths.temporaryStoragePath);

        // Load the settings to the global SettingsCache
        await loadSettings(serverPaths.settingsFile);

        log.info('app is ready');

        const { h5pEditor, h5pPlayer } = await initH5P(serverPaths, true);
        log.info('initialized h5p');

        const expressApp = await createExpressApp(
            h5pEditor,
            h5pPlayer,
            serverPaths,
            () => windowManager.getFocusedWindow().browserWindow,
            () => windowManager
        );
        log.info('initialized express app');

        const server = await createHttpServer(
            expressApp,
            process.env.PORT ? Number.parseInt(process.env.PORT, 10) : 0
        );
        log.info('initialized HTTP server');

        port = (server.address() as any).port;
        log.info(`port is ${port}`);

        websocket = createWebsocket(server);
        log.info('websocket created on server');

        const createEmptyWindow = windowManager.init(
            port,
            websocket,
            (contentId) => h5pEditor.deleteContent(contentId, new User())
        );
        log.info('WindowManager initialized');

        // Parse command like arguments
        const argv = process.argv;
        if (process.platform === 'win32' && argv.length >= 2) {
            // Check if there are H5Ps specified in the command line args and
            // load them (Windows only).
            argv.splice(0, 1);
            const openFilePaths = argv.filter((arg) => arg.endsWith('.h5p'));
            if (openFilePaths.length > 0) {
                log.debug(`Opening file(s): ${openFilePaths.join(' ')}`);
                for (const file of openFilePaths) {
                    windowManager.openH5P(file, false);
                }
            } else {
                windowManager.addWindow();
            }
        } else if (createEmptyWindow) {
            windowManager.addWindow();
        }
        updateMenu('/', windowManager);
        initUpdater(serverPaths);
        log.info('updater started');
    });

    app.on('will-quit', (event) => {
        log.debug(`Deleting temporary directory: ${tmpDir}`);
        fsExtra.removeSync(tmpDir);
    });

    app.on('before-quit', (event) => {
        log.debug('Received before-quit event');
        if (process.platform !== 'darwin') {
            if (windowManager.getWindowCount() > 1) {
                log.debug('Closing current window.');
                windowManager.getFocusedWindow()?.browserWindow.close();
                event.preventDefault();
            } else {
                log.debug('Quitting application.');
            }
        }
    });
}

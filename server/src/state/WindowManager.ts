import electron, { BrowserWindow } from 'electron';
import i18next from 'i18next';
import SocketIO from 'socket.io';
import Window from './Window';
import log from 'electron-log';

/**
 * Keeps track of windows. Also manages delayed window opening for macOS.
 */
export default class WindowManager {
    constructor(
        private isDevelopment: boolean,
        private focusChangedCallback: (focusedWindow: Window) => Promise<void>,
        private contentInFocusedWindowChanged: (
            focusedWindow: Window,
            contentId: string,
            path: string
        ) => Promise<void>
    ) {
        this.windows = {};
    }

    private deleteContentCallback: (contentId: string) => Promise<void>;
    private focusedWindow: Window;
    private initialized: boolean = false;
    private port: number;
    private queuedH5PPaths: string[] = [];
    private websocket: SocketIO.Server;
    private windows: { [key: string]: Window };

    public addWindow(): Window {
        const window = new Window(
            this.websocket,
            this.port,
            async (windowId: string) => {
                // Executed when the window was closed
                const w = this.windows[windowId];
                if (w.getContentId() && this.deleteContentCallback) {
                    log.debug(
                        `Deleting temporary files with contentId ${w.getContentId()}`
                    );
                    await this.deleteContentCallback(w.getContentId());
                }
                delete this.windows[windowId];

                // Set focus to another H5P player window
                for (const wind of Object.values(this.windows)) {
                    const browsWind = wind?.browserWindow;
                    if (!browsWind || browsWind.isMinimized()) {
                        continue;
                    }
                    browsWind.focus();
                    return;
                }

                // No other H5P Player window open, so another app has focus
                this.focusedWindow = undefined;
                if (this.focusChangedCallback) {
                    await this.focusChangedCallback(null);
                }
            },
            this.isDevelopment,
            async (windowId, contentId, path) => {
                if (this.contentInFocusedWindowChanged) {
                    await this.contentInFocusedWindowChanged(
                        this.windows[windowId],
                        contentId,
                        path
                    );
                }
            },
            this
        );
        this.windows[window.getWindowId()] = window;

        window.browserWindow.on('focus', async () => {
            this.focusedWindow = window;
            if (this.focusChangedCallback) {
                await this.focusChangedCallback(window);
            }
        });

        if (this.getWindowCount() === 1) {
            this.focusedWindow = window;
        }

        return window;
    }

    public createSpecialWindow(uri: string): void {
        const specialWindow = new BrowserWindow();
        specialWindow.webContents.on('will-navigate', (e, url) => {
            e.preventDefault();
            electron.shell.openExternal(url);
        });

        specialWindow.setMenu(
            electron.Menu.buildFromTemplate([
                { label: i18next.t('lumi:menu.file.close'), role: 'close' }
            ])
        );
        specialWindow.loadURL(`http://localhost:${this.port}/${uri}`);
    }

    public getFocusedWindow(): Window | undefined {
        return this.focusedWindow;
    }

    public getWindow(windowId: string): Window | undefined {
        return this.windows[windowId];
    }

    public getWindowCount(): number {
        return Object.keys(this.windows).length;
    }

    /**
     *
     * @param port
     * @param websocket
     * @returns true if a new window should be created
     */
    public init(
        port: number,
        websocket: SocketIO.Server,
        deleteContentCallback: (contentId: string) => Promise<void>
    ): boolean {
        this.port = this.isDevelopment ? 3000 : port;
        this.websocket = websocket;
        this.deleteContentCallback = deleteContentCallback;
        this.initialized = true;
        if (this.queuedH5PPaths.length > 0) {
            for (const file of this.queuedH5PPaths) {
                this.openH5P(file, false);
            }
            return false;
        }
        return true;
    }

    /**
     * Opens an H5P file either in a new window or in the same window, depending
     * on the parameters. Brings the window into focus if the H5P is already
     * opened.
     * @param file
     * @param sameWindow
     */
    public openH5P(file: string, sameWindow: boolean): void {
        if (!this.initialized) {
            log.debug(
                'App not fully initialized yet. Queuing file open request.'
            );
            this.queuedH5PPaths.push(file);
        } else {
            if (!this.restoreWindowForFile(file)) {
                log.debug('Window could not be restored.');
                if (sameWindow && this.focusedWindow) {
                    log.debug('Loading in same window.');
                    this.focusedWindow.loadH5P(file);
                } else {
                    log.debug('Creating new window.');
                    const window = this.addWindow();
                    window.loadH5P(file);
                }
            }
        }
    }

    public restoreOrNewWindow(): void {
        const focusedWindow = this.getFocusedWindow();
        if (focusedWindow) {
            if (focusedWindow.browserWindow.isMinimized()) {
                focusedWindow.browserWindow.restore();
            }
            focusedWindow.browserWindow.focus();
        } else {
            this.addWindow();
        }
    }

    public sendWebsocketMessageToFocusedWindow(action: {
        payload: any;
        type: string;
    }): void {
        const focusedWindow = this.getFocusedWindow();
        if (!focusedWindow) {
            return;
        }
        focusedWindow.emitAction(action);
    }

    /**
     * Checks if the file is already open in one window and if so, brings the
     * window to the front.
     * @param path
     * @returns true if a window was found and restored, false if none was found
     */
    private restoreWindowForFile(path: string): boolean {
        for (const window of Object.values(this.windows)) {
            if (window.getPath() === path) {
                if (window.browserWindow.isMinimized()) {
                    window.browserWindow.restore();
                }
                window.browserWindow.focus();
                return true;
            }
        }
        return false;
    }
}

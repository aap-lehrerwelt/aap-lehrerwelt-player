interface IElectronState {
    blockKeyboard: boolean;
}

/**
 * Keeps track of global states that must be shared between server and client.
 */
class StateStorage {
    constructor() {
        this.state = {
            blockKeyboard: false
        };
    }
    public state: IElectronState;

    getState(): IElectronState {
        return this.state;
    }

    setState(s: IElectronState): void {
        this.state = s;
    }
}

export default new StateStorage();

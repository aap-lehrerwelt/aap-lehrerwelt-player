import electron, {
    BrowserWindow,
    BrowserWindowConstructorOptions
} from 'electron';
import SocketIO from 'socket.io';
import { nanoid } from 'nanoid';
import DelayedEmitter from '../helpers/DelayedEmitter';
import WindowManager from './WindowManager';

export default class Window {
    constructor(
        websocket: SocketIO.Server,
        port: number,
        private closeCallback: (windowId: string) => Promise<void>,
        isDevelopment: boolean,
        private contentChangedCallback: (
            windowId: string,
            contentId: string,
            path: string
        ) => Promise<void>,
        windowManager: WindowManager
    ) {
        this.id = nanoid();

        let windowOptions: BrowserWindowConstructorOptions = {
            show: false,
            width: 1360,
            height: 1000,
            minWidth: 700,
            minHeight: 480
        };

        if (windowManager.getFocusedWindow()) {
            const position = windowManager
                .getFocusedWindow()
                .browserWindow.getPosition();

            windowOptions = {
                ...windowOptions,
                x: position[0] + 25,
                y: position[1] + 25
            };
        }
        this.browserWindow = new BrowserWindow(windowOptions);
        this.browserWindow.show();

        this.delayedEmitter = new DelayedEmitter(websocket.of(`/${this.id}`));
        this.browserWindow.loadURL(
            `http://localhost:${port}?windowId=${this.id}`
        );

        if (isDevelopment) {
            this.browserWindow.webContents.openDevTools();
        }

        this.browserWindow.on('closed', () => {
            this.browserWindow = null;
            this.delayedEmitter = null;
            return this.closeCallback(this.id);
        });

        this.browserWindow.webContents.on('will-navigate', (e, url) => {
            if (url.startsWith('http')) {
                e.preventDefault();
                electron.shell.openExternal(url);
            }
        });

        this.browserWindow.webContents.on('new-window', (e, url) => {
            if (url.startsWith('http')) {
                e.preventDefault();
                electron.shell.openExternal(url);
            }
        });
    }

    public browserWindow: BrowserWindow;
    public delayedEmitter: DelayedEmitter;
    public id: string;
    public socket: SocketIO.Socket;

    private contentId: string;
    private path: string;

    public emitAction(action: { payload: any; type: string }): void {
        this.delayedEmitter.emit('action', {
            type: 'action',
            payload: action
        });
    }

    public getContentId(): string {
        return this.contentId;
    }

    public getPath(): string {
        return this.path;
    }

    public getWindowId(): string {
        return this.id;
    }

    public async loadH5P(file: string): Promise<void> {
        this.delayedEmitter.emit('action', {
            payload: {
                paths: [file]
            },
            type: 'OPEN_H5P'
        });
    }

    public async setContent(path: string, contentId: string): Promise<void> {
        this.path = path;
        this.contentId = contentId;
        if (this.contentChangedCallback) {
            this.contentChangedCallback(this.id, contentId, path);
        }
    }
}

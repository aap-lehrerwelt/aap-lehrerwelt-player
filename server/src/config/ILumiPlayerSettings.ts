/**
 * Settings configurable in the app by the user.
 */
export default interface ILumiPlayerSettings {
    autoUpdates: boolean;
    bugTracking: boolean;
    firstOpen: boolean;
    language: string;
    lastVersion: string;
    licenseConsent: boolean;
    usageStatistics: boolean;
}

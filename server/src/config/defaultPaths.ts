import path from 'path';
import IPaths from './IPaths';

/**
 * Creates the paths required for H5P to work.
 * @param userData a directory which is scoped per user and persistent
 * @param tempDir a temporary directory whose content can be deleted after the
 * app was closed
 * @returns
 */
export default function (userData: string, tempDir: string): IPaths {
    return {
        librariesPath: path.join(tempDir, 'libraries'),
        settingsFile: path.join(userData, 'settings.json'),
        temporaryStoragePath: path.join(tempDir, 'tmp'),
        contentPath: path.join(tempDir, 'content')
    };
}

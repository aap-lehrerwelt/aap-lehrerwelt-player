import defaultSettings from './defaultSettings';
import ILumiPlayerSettings from './ILumiPlayerSettings';

/**
 * Keeps the Lumi Player settings in memory to avoid repeated disk access.
 * Singleton.
 */
class SettingsCache {
    constructor() {
        this.settings = defaultSettings;
    }
    public settings: ILumiPlayerSettings;

    private subscribers: (() => void)[] = [];

    getSettings(): ILumiPlayerSettings {
        return this.settings;
    }

    setSettings(s: ILumiPlayerSettings): void {
        this.settings = s;
        this.subscribers.forEach((subscriber) => subscriber());
    }

    subscribe(handler: () => void): void {
        this.subscribers.push(handler);
    }

    unsubscribe(handler: () => void): void {
        const index = this.subscribers.indexOf(handler);
        if (index >= 0) {
            this.subscribers.splice(index, 1);
        }
    }
}

export default new SettingsCache();

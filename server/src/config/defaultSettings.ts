import { app } from 'electron';

import ILumiPlayerSettings from './ILumiPlayerSettings';

const defaultSettings: ILumiPlayerSettings = {
    autoUpdates: false,
    bugTracking: false,
    firstOpen: true,
    language: 'de',
    lastVersion: app ? app.getVersion() : 'test',
    licenseConsent: false,
    usageStatistics: false
};
export default defaultSettings;

import electron from 'electron';
import { IH5PConfig } from '@lumieducation/h5p-server';

/**
 * Hard-codes configuration options and literals that are used by H5P and that
 * are not user-configurable. We don't want any configurable H5P settings, so we
 * hard-code all of them here. This makes later updates easier.
 */
export default class H5PConfig implements IH5PConfig {
    public ajaxUrl: string = '/ajax';
    public baseUrl: string = '/api/v1/h5p';
    public contentFilesUrl: string = '/content';
    public contentFilesUrlPlayerOverride: string;
    public contentHubContentEndpoint: string = '';
    public contentHubEnabled: boolean = false;
    public contentHubMetadataEndpoint: string = '';
    public contentHubMetadataRefreshInterval: number = 1 * 1000 * 60 * 60 * 24;
    public contentTypeCacheRefreshInterval: number = 1 * 1000 * 60 * 60 * 24;
    public contentUserDataUrl: string = '/contentUserData';
    public contentWhitelist: string =
        'json png jpg jpeg gif bmp tif tiff svg eot ttf woff woff2 otf webm mp4 ogg mp3 m4a wav txt pdf rtf doc docx xls xlsx ppt pptx odt ods odp xml csv diff patch swf md textile vtt webvtt';
    public coreApiVersion: { major: number; minor: number } = {
        major: 1,
        minor: 24
    };
    public coreUrl: string = '/core';
    public customization: {
        global: {
            editor?: {
                scripts?: string[];
                styles?: string[];
            };
            player?: {
                scripts?: string[];
                styles?: string[];
            };
        };
    } = {
        global: {
            editor: {
                scripts: [],
                styles: []
            },
            player: {
                scripts: [],
                styles: []
            }
        }
    };
    public disableFullscreen: boolean = false;
    public downloadUrl: string = '/download';
    public editorAddons?: {
        [machineName: string]: string[];
    };
    public editorLibraryUrl: string = '/editor';
    public enableLrsContentTypes: boolean = true;
    public exportMaxContentPathLength: number = 255;
    public fetchingDisabled: 0 | 1 = 0;
    public h5pVersion: string = '1.24-master';
    public hubContentTypesEndpoint: string = '';
    public hubRegistrationEndpoint: string = '';
    public installLibraryLockMaxOccupationTime: number = 30000;
    public installLibraryLockTimeout: number = 50000;
    public librariesUrl: string = '/libraries';
    public libraryConfig: { [machineName: string]: any };
    public libraryWhitelist: string = 'js css';
    public lrsContentTypes: string[] = [
        'H5P.Questionnaire',
        'H5P.FreeTextQuestion'
    ];
    public maxFileSize: number = 2048 * 1024 * 1024;
    public maxTotalSize: number = 2048 * 1024 * 1024;
    public paramsUrl: string = '/params';
    public platformName: string = 'Lumi Player';
    public platformVersion: string = electron.app?.getVersion() ?? '0.1';
    public playerAddons?: {
        [machineName: string]: string[];
    };
    public playUrl: string = '/play';
    public proxy?: {
        host: string;
        port: number;
        protocol?: 'http' | 'https';
    };
    public sendUsageStatistics: boolean = false;
    public setFinishedUrl: string = '/setFinished';
    public siteType: 'local' | 'network' | 'internet' = 'local';
    public temporaryFileLifetime: number = 10000; // 5 minutes
    public temporaryFilesUrl: string = '/temp-files';
    public uuid: string = '';

    /**
     * As we hard-code all H5P settings in this file, we don't load anything
     * from the disk.
     */
    public async load(): Promise<H5PConfig> {
        return this;
    }

    /**
     * As we hard-code all H5P settings in this file, we don't save any changes.
     */
    public async save(): Promise<void> {
        return;
    }
}

/**
 * Paths to directories and paths on the disk.
 */
export default interface IPaths {
    /**
     * The directory at which content files are stored while a h5p is opened.
     * (transient)
     */
    contentPath: string;
    /**
     * The directory at which libraries are stored while the app is open.
     * (transient)
     */
    librariesPath: string;
    /**
     * The JSON file in which the user's app settings are stored. (persistent,
     * user-based)
     */
    settingsFile: string;
    /**
     * The directory at which temporary files (= media files when opening .h5p
     * packages) are stored. (transient)
     */
    temporaryStoragePath: string;
}

import express from 'express';

import { platformSupportsUpdates } from '../boot/updater';

export type Platform =
    | 'mac'
    | 'mas'
    | 'win'
    | 'win-store'
    | 'linux'
    | NodeJS.Platform;

export function getPlatform(): Platform {
    if (process.mas) {
        return 'mas';
    }

    if (process.windowsStore) {
        return 'win-store';
    }

    if (process.platform === 'darwin') {
        return 'mac';
    }

    return process.platform;
}

export default function (): express.Router {
    const router = express.Router();

    router.get(
        `/`,
        async (
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            try {
                res.status(200).json({
                    platformSupportsUpdates: platformSupportsUpdates(),
                    platform: getPlatform()
                });
            } catch (error) {
                res.status(500).json(error);
            }
        }
    );

    return router;
}

import express from 'express';
import { autoUpdater } from 'electron-updater';
import { app } from 'electron';

import log from 'electron-log';

export default function (): express.Router {
    const router = express.Router();
    router.get(
        `/`,
        async (
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            try {
                const updateCheckResult = await autoUpdater.checkForUpdates();
                if (updateCheckResult.updateInfo.version !== app.getVersion()) {
                    res.status(200).json(updateCheckResult.updateInfo);
                } else {
                    res.status(200).json({});
                }
            } catch (error) {
                res.status(500).end();
            }
        }
    );

    router.post(
        `/`,
        async (
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            try {
                autoUpdater.on('update-downloaded', async () => {
                    log.debug('Installing update now');
                    autoUpdater.quitAndInstall();
                });
                await autoUpdater.downloadUpdate();
            } catch (error) {
                res.status(500).end();
            }
        }
    );

    return router;
}

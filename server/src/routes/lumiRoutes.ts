import express from 'express';
import { H5PEditor } from '@lumieducation/h5p-server';
import LumiController from '../controllers/LumiController';
import { BrowserWindow } from 'electron';
import { errorHandler } from '@lumieducation/h5p-express/build/expressErrorHandler';
import WindowManager from '../state/WindowManager';

export default function (
    h5pEditor: H5PEditor,
    getBrowserWindow: () => BrowserWindow,
    getWindowManager: () => WindowManager
): express.Router {
    const router = express.Router();
    const lumiController = new LumiController(
        h5pEditor,
        getBrowserWindow,
        getWindowManager
    );

    router.get(
        '/selectFiles',
        (
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            lumiController
                .selectFiles()
                .then((result) => {
                    res.status(200).json(result);
                })
                .catch((error) => {
                    next(error);
                });
        }
    );

    router.post(
        `/import`,
        (
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            lumiController
                .importH5pFile(
                    req.body.path,
                    req.body.windowId,
                    req.body.activeContentId
                )
                .then((result) => {
                    res.status(200).json(result);
                })
                .catch((error) => {
                    errorHandler(req.language)(error, req, res, next);
                });
        }
    );

    router.post('/showLicense', (req, res) => {
        getWindowManager()?.createSpecialWindow('license.html');
        res.status(200).send();
    });

    router.post('/showPrivacyPolicy', (req, res) => {
        getWindowManager()?.createSpecialWindow('privacy.html');
        res.status(200).send();
    });

    return router;
}

import express from 'express';
import fsExtra from 'fs-extra';
import IPaths from '../config/IPaths';
import i18next from 'i18next';

import settingsCache from '../config/SettingsCache';

export default function (serverPaths: IPaths): express.Router {
    const router = express.Router();
    router.get(
        `/`,
        async (
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            try {
                const settings = await fsExtra.readJSON(
                    serverPaths.settingsFile
                );

                res.status(200).json(settings);
            } catch (error) {
                res.status(500).end();
            }
        }
    );

    router.patch(
        '/',
        async (
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            try {
                if (req.body) {
                    const oldSettings = settingsCache.getSettings();
                    await fsExtra.writeJSON(serverPaths.settingsFile, req.body);

                    if (
                        req.body.language &&
                        req.body.language !== oldSettings.language
                    ) {
                        await i18next.loadLanguages(req.body.language);
                        await i18next.changeLanguage(req.body.language);
                    }

                    settingsCache.setSettings(req.body);

                    res.status(200).json(req.body);
                } else {
                    throw new Error('Body container no contents.');
                }
            } catch (error) {
                res.status(500).end();
            }
        }
    );

    return router;
}

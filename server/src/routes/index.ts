import express from 'express';
import { BrowserWindow } from 'electron';

import { H5PEditor, H5PPlayer } from '@lumieducation/h5p-server';
import { h5pAjaxExpressRouter } from '@lumieducation/h5p-express';

import lumiRoutes from './lumiRoutes';
import Logger from '../helpers/Logger';
import IPaths from '../config/IPaths';
import h5pRoutes from './h5pRoutes';
import settingsRoutes from './settingsRoutes';
import systemRoutes from './systemRoutes';
import updatesRoutes from './updatesRoutes';

import User from '../h5pImplementations/User';
import WindowManager from '../state/WindowManager';

const log = new Logger('routes');

export default function (
    h5pEditor: H5PEditor,
    h5pPlayer: H5PPlayer,
    serverPaths: IPaths,
    getBrowserWindow: () => BrowserWindow,
    getWindowManager: () => WindowManager
): express.Router {
    const router = express.Router();

    log.info('setting up routes');

    // Adding dummy user to make sure all requests can be handled
    router.use((req, res, next) => {
        (req as any).user = new User();
        next();
    });

    router.use('/api/v1/system', systemRoutes());
    router.use('/api/v1/updates', updatesRoutes());
    router.use('/api/v1/settings', settingsRoutes(serverPaths));

    // The Express adapter handles GET and POST requests to various H5P
    // endpoints. You can add an options object as a last parameter to configure
    // which endpoints you want to use. In this case we don't pass an options
    // object, which means we get all of them.
    router.use(
        h5pEditor.config.baseUrl,
        h5pAjaxExpressRouter(
            h5pEditor,
            `${__dirname}/../../../h5p/core`,
            // the path on the local disc where the files of the JavaScript
            // client of the player are stored
            `${__dirname}/../../../h5p/core`,
            // the path on the local disc where the files of the JavaScript
            // client of the editor are stored; we don't need the editor, so
            // there are no editor files here
            {
                handleErrors: true,
                routeCoreFiles: true,
                routeEditorCoreFiles: false,
                routeGetAjax: true,
                routeGetContentFile: true,
                routeGetDownload: false,
                routeGetLibraryFile: true,
                routeGetParameters: false,
                routeGetTemporaryContentFile: false,
                routePostAjax: false
            },
            'auto' // You can change the language of the editor here by setting
            // the language code you need here. 'auto' means the route will try
            // to use the language detected by the i18next language detector.
        )
    );

    router.use('/locales', express.static(`${__dirname}/../../../locales`));

    router.use(h5pEditor.config.baseUrl, h5pRoutes(h5pPlayer));

    router.use(
        '/api/v1/lumi',
        lumiRoutes(h5pEditor, getBrowserWindow, getWindowManager)
    );

    router.get('*', express.static(`${__dirname}/../../client`));

    return router;
}

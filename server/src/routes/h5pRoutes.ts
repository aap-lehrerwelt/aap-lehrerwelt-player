import express from 'express';
import _path from 'path';
import * as H5P from '@lumieducation/h5p-server';
import i18next from 'i18next';

import User from '../h5pImplementations/User';

export default function (h5pPlayer: H5P.H5PPlayer): express.Router {
    const router = express.Router();

    router.get(`/:contentId/play`, async (req, res) => {
        try {
            const content = (await h5pPlayer.render(
                req.params.contentId,
                new User(),
                i18next.language
            )) as H5P.IPlayerModel;
            // We override the embed types to make sure content always resizes
            // properly.
            content.embedTypes = ['iframe'];
            res.send(content);
            res.status(200).end();
        } catch (error) {
            res.status(500).end(error.message);
        }
    });

    return router;
}

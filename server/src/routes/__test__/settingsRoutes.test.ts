import request from 'supertest';
import path from 'path';
import express from 'express';
import fsExtra from 'fs-extra';
import { dialog, BrowserWindow, MessageBoxOptions } from 'electron';

import createExpressApp from '../../boot/expressApp';
import initH5P from '../../boot/h5p';

describe('GET /settings', () => {
    let app: express.Application;

    beforeAll(async () => {
        const serverPaths = {
            librariesPath: path.resolve('test', 'data', `libraries`),
            temporaryStoragePath: path.resolve('test', 'data', 'tmp'),
            contentPath: path.resolve('test', 'data', 'content'),
            settingsFile: path.resolve('test', 'data', 'settings.json')
        };
        const { h5pEditor, h5pPlayer } = await initH5P(serverPaths, false);
        app = await createExpressApp(
            h5pEditor,
            h5pPlayer,
            serverPaths,
            null,
            null
        );
        return app;
    });
    it('should return the settings', async (done) => {
        const settings = await fsExtra.readJSON(
            path.resolve('test', 'data', 'settings.json')
        );

        const res = await request(app).get('/api/v1/settings');
        expect(res.statusCode).toEqual(200);
        expect(res.body).toStrictEqual(settings);
        done();
    });
});

describe('PATCH /settings', () => {
    let app: express.Application;

    beforeAll(async () => {
        const serverPaths = {
            librariesPath: path.resolve('test', 'data', `libraries`),
            temporaryStoragePath: path.resolve('test', 'data', 'tmp'),
            contentPath: path.resolve('test', 'data', 'content'),
            settingsFile: path.resolve('test', 'data', 'settings.json')
        };
        const { h5pEditor, h5pPlayer } = await initH5P(serverPaths, false);
        app = await createExpressApp(
            h5pEditor,
            h5pPlayer,
            serverPaths,
            null,
            null
        );
        return app;
    });
    it('should update the settings', async (done) => {
        dialog.showMessageBox = (async (
            browserWindow: BrowserWindow,
            options: MessageBoxOptions
        ) => {
            return {
                response: 1,
                checkboxChecked: false,
                checkboxLabel: false
            };
        }) as any;

        const settings = await fsExtra.readJSON(
            path.resolve('test', 'data', 'settings.json')
        );

        const updatedSettings = {
            ...settings,
            test: 'abc'
        };
        const res = await request(app)
            .patch('/api/v1/settings')
            .send(updatedSettings);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toStrictEqual(updatedSettings);

        expect(
            await fsExtra.readJSON(
                path.resolve('test', 'data', 'settings.json')
            )
        ).toStrictEqual(updatedSettings);
        done();
    });
});

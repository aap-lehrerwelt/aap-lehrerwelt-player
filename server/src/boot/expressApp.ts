import * as H5P from '@lumieducation/h5p-server';
import { BrowserWindow } from 'electron';
import express from 'express';
import i18next from 'i18next';
import i18nextHttpMiddleware from 'i18next-http-middleware';

import LumiError from '../helpers/LumiError';
import routes from '../routes';
import IPaths from '../config/IPaths';
import User from '../h5pImplementations/User';
import settingsCache from '../config/SettingsCache';
import bodyParser from 'body-parser';
import WindowManager from '../state/WindowManager';

/**
 * The Express app that's the root of the NodeJS server running inside Electron.
 */
export default async (
    h5pEditor: H5P.H5PEditor,
    h5pPlayer: H5P.H5PPlayer,
    serverPaths: IPaths,
    getBrowserWindow: () => BrowserWindow,
    getWindowManager: () => WindowManager,
    options?: {}
) => {
    const app = express();

    app.use(bodyParser.json({ limit: h5pEditor.config.maxTotalSize }));
    app.use(
        bodyParser.urlencoded({
            extended: true,
            limit: h5pEditor.config.maxTotalSize
        })
    );

    app.use(
        (
            req: express.Request,
            res: express.Response,
            next: express.NextFunction
        ) => {
            res.set(
                'Content-Security-Policy',
                "default-src 'self' data: 'unsafe-inline' 'unsafe-hashes' 'unsafe-eval'"
            );
            (req as any).user = new User();
            next();
        }
    );

    // The i18nextExpressMiddleware injects the function t(...) into the req
    // object.
    app.use(i18nextHttpMiddleware.handle(i18next));

    app.use(async (req: any, res: any, next: express.NextFunction) => {
        const languageCode = settingsCache.getSettings().language;
        req.language = languageCode;
        req.languages = [languageCode];
        next();
    });

    app.use(
        '/',
        routes(
            h5pEditor,
            h5pPlayer,
            serverPaths,
            getBrowserWindow,
            getWindowManager
        )
    );

    app.use((error, req, res, next) => {
        res.status(error.status || 500).json(
            new LumiError(error.code, error.message, error.status)
        );
    });
    return app;
};

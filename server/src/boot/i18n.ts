import i18next, { TFunction } from 'i18next';
import i18nextBackend from 'i18next-node-fs-backend';

/**
 * Initializes i18next.
 * @param serverPaths
 * @returns
 */
export default async function bootI18n(): Promise<TFunction> {
    const t = await i18next.use(i18nextBackend).init({
        lng: 'de', // languageCode,
        fallbackLng: 'de',
        ns: [
            'client',
            'copyright-semantics',
            'hub',
            'library-metadata',
            'lumi',
            'metadata-semantics',
            'server',
            'storage-file-implementations'
        ],
        load: 'languageOnly',
        defaultNS: 'server',
        backend: {
            loadPath: `${__dirname}/../../../locales/{{ns}}/{{lng}}.json`
        }
    });

    return t;
}

import SocketIO from 'socket.io';
import http from 'http';

import Logger from '../helpers/Logger';

const log = new Logger('websocket');

let globalWebsocket: SocketIO.Server;

export { globalWebsocket };

/**
 * Initializes the websocket which is used to send events from the server to the
 * client (e.g. Redux actions to open files).
 * @param server
 * @returns
 */
export default function (server: http.Server): SocketIO.Server {
    log.info('booting');
    globalWebsocket = new SocketIO.Server(server);
    globalWebsocket.on('connection', (socket: SocketIO.Socket) => {
        log.info('new connection');

        socket.on('dispatch', (action) => {
            log.info(action);

            globalWebsocket.emit('action', action);
        });
    });

    return globalWebsocket;
}

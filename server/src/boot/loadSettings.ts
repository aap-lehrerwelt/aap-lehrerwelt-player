import fsExtra from 'fs-extra';
import { app } from 'electron';

import defaultSettings from '../config/defaultSettings';
import settingsCache from '../config/SettingsCache';

/**
 * Loads the settings from the disk or creates a new settings file if it doesn't
 * exist. The settings will then be available in the global SettingsCache.
 * @param settingsFile
 */
export default async function loadSettings(
    settingsFile: string
): Promise<void> {
    try {
        // Check if current settings exists and is read- and parsable
        let settingOk = false;
        try {
            if (await fsExtra.pathExists(settingsFile)) {
                await fsExtra.readJSON(settingsFile);
                settingOk = true;
            }
        } catch (error) {
            settingOk = false;
        }

        if (!settingOk) {
            await fsExtra.writeJSON(settingsFile, {
                ...defaultSettings,
                language: app.getLocale()
            });
        }

        const checkSettings = await fsExtra.readJSON(settingsFile);

        if (!checkSettings.language) {
            fsExtra.writeJSON(settingsFile, {
                ...checkSettings,
                language: app.getLocale()
            });
        }

        settingsCache.setSettings(await fsExtra.readJSON(settingsFile));
    } catch (error) {
        throw error;
    }
}

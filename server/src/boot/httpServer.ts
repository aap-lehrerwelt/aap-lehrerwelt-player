import http from 'http';
import Logger from '../helpers/Logger';

const log = new Logger('boot');

/**
 * Creates a basic HTTP server with the Express app.
 */
export default async (
    app: Express.Application,
    port: number
): Promise<http.Server> => {
    log.debug(`Creating HTTP server on port ${port}.`);
    const server = http.createServer(app);
    return new Promise((res, rej) => {
        server.listen(port, 'localhost', 511, () => {
            log.info(`server booted on port ${(server.address() as any).port}`);
            res(server);
        });
    });
};

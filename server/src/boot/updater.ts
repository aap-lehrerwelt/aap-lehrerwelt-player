import { autoUpdater } from 'electron-updater';

import IPaths from '../config/IPaths';
import log from 'electron-log';
import SettingsCache from '../config/SettingsCache';

export const platformSupportsUpdates = () => {
    if (process.env.DISABLE_UPDATES) {
        return false;
    }
    if (process.platform === 'win32') {
        return !process.windowsStore;
    }
    if (process.platform === 'darwin') {
        return !process.mas;
    }
    if (process.platform === 'linux') {
        if (process.env.APPIMAGE) {
            return true;
        }
    }
    return false;
};

const configureElectronUpdate = () => {
    if (process.platform === 'darwin') {
        autoUpdater.autoDownload = true;
        autoUpdater.autoInstallOnAppQuit = true;
    } else {
        autoUpdater.autoDownload = false;
        autoUpdater.autoInstallOnAppQuit = false;
    }
};

/**
 * Initializes the updater included in electron-builder.
 * @param app
 * @param websocket
 * @param serverPaths
 */
export default async function initUpdater(serverPaths: IPaths): Promise<void> {
    configureElectronUpdate();
    autoUpdater.logger = log;

    if (platformSupportsUpdates() && SettingsCache.getSettings().autoUpdates) {
        autoUpdater.checkForUpdatesAndNotify();
    }

    SettingsCache.subscribe(() => {
        if (SettingsCache.getSettings().autoUpdates) {
            log.debug('Enabling auto updates');
            configureElectronUpdate();
        } else {
            log.debug('Disabling auto updates');
            autoUpdater.autoDownload = false;
            autoUpdater.autoInstallOnAppQuit = false;
        }
    });
}

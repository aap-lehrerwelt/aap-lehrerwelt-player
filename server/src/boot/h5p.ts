import { H5PEditor, H5PPlayer } from '@lumieducation/h5p-server';
import H5PConfig from '../config/H5PConfig';

import IPaths from '../config/IPaths';
import createH5PEditor from './h5pEditor';
import bootI18n from './i18n';
import Logger from '../helpers/Logger';

const log = new Logger('h5pRoot');

const cleanUpInterval = 120000; // 2 min

const cleanUpTemporaryFiles = async (h5pEditor: H5PEditor) => {
    log.debug('Cleaning up temporary H5P files');
    try {
        await h5pEditor.temporaryFileManager.cleanUp();
    } catch (error) {
        log.error(`Error while deleting temporary files. ${error}`);
    }
    log.debug(
        `Cleaned up temporary H5P files. Next cleanup in ${cleanUpInterval} ms.`
    );
    setTimeout(() => cleanUpTemporaryFiles(h5pEditor), cleanUpInterval);
};

export default async function initH5P(
    serverPaths: IPaths,
    enableCleanup: boolean
): Promise<{ h5pEditor: H5PEditor; h5pPlayer: H5PPlayer }> {
    const config = new H5PConfig();

    const translationFunction = await bootI18n();

    // The H5PEditor object is central to all operations of
    // @lumieducation/h5p-server. We need it in even the player as we need a way
    // of "uploading" h5p packages.
    const h5pEditor: H5PEditor = await createH5PEditor(
        config,
        serverPaths.librariesPath, // the path on the local disc where libraries should be stored)
        serverPaths.contentPath, // the path on the local disc where content is stored. Only used / necessary if you use the local filesystem content storage class.
        serverPaths.temporaryStoragePath, // the path on the local disc where temporary files (uploads) should be stored. Only used / necessary if you use the local filesystem temporary storage class.
        (key, language) => translationFunction(key, { lng: language })
    );

    const h5pPlayer = new H5PPlayer(
        h5pEditor.libraryStorage,
        h5pEditor.contentStorage,
        config,
        undefined,
        undefined,
        (key, language) => translationFunction(key, { lng: language })
    );
    h5pPlayer.setRenderer((model) => model);

    if (enableCleanup) {
        setTimeout(() => cleanUpTemporaryFiles(h5pEditor), cleanUpInterval);
        log.debug(`Next H5P temporary file cleanup in ${cleanUpInterval} ms.`);
    }

    return { h5pEditor, h5pPlayer };
}

import i18next from 'i18next';
import WindowManager from '../state/WindowManager';
const isMac = process.platform === 'darwin';

export default function (windowManager: WindowManager): any {
    const h5pOpenInWindow =
        windowManager.getFocusedWindow()?.getContentId() !== undefined;

    return !isMac && !h5pOpenInWindow
        ? []
        : [
              {
                  label: i18next.t('lumi:menu.view.label'),
                  submenu: [
                      h5pOpenInWindow
                          ? {
                                label: i18next.t('lumi:menu.view.resetZoom'),
                                click: () => {
                                    windowManager.sendWebsocketMessageToFocusedWindow(
                                        {
                                            type: 'H5P_ZOOM_RESET',
                                            payload: {}
                                        }
                                    );
                                }
                            }
                          : undefined,
                      h5pOpenInWindow
                          ? {
                                accelerator: 'CmdOrCtrl+Plus',
                                label: i18next.t('lumi:menu.view.zoomIn'),
                                click: () => {
                                    windowManager.sendWebsocketMessageToFocusedWindow(
                                        {
                                            type: 'H5P_ZOOM_IN',
                                            payload: {}
                                        }
                                    );
                                }
                            }
                          : undefined,
                      // We have a second zoom in menu entry as Electron has a
                      // bug when setting Ctrl + Plus on German keyboards. See:
                      // https://github.com/electron/electron/issues/6731
                      h5pOpenInWindow
                          ? {
                                accelerator: 'CmdOrCtrl+=',
                                label: i18next.t('lumi:menu.view.zoomIn'),
                                click: () => {
                                    windowManager.sendWebsocketMessageToFocusedWindow(
                                        {
                                            type: 'H5P_ZOOM_IN',
                                            payload: {}
                                        }
                                    );
                                },
                                visible: false
                            }
                          : undefined,
                      h5pOpenInWindow
                          ? {
                                accelerator: 'CmdOrCtrl+-',
                                label: i18next.t('lumi:menu.view.zoomOut'),
                                click: () => {
                                    windowManager.sendWebsocketMessageToFocusedWindow(
                                        {
                                            type: 'H5P_ZOOM_OUT',
                                            payload: {}
                                        }
                                    );
                                }
                            }
                          : undefined,
                      h5pOpenInWindow ? { type: 'separator' } : undefined,
                      h5pOpenInWindow
                          ? {
                                label: i18next.t(
                                    'lumi:menu.view.contentWidth.header'
                                ),
                                submenu: [
                                    {
                                        label: i18next.t(
                                            'lumi:menu.view.contentWidth.narrow'
                                        ),
                                        accelerator: 'CmdOrCtrl+1',
                                        click: () => {
                                            windowManager.sendWebsocketMessageToFocusedWindow(
                                                {
                                                    type: 'H5P_SET_WIDTH_LIMIT',
                                                    payload: {
                                                        limit: 1
                                                    }
                                                }
                                            );
                                        }
                                    },
                                    {
                                        label: i18next.t(
                                            'lumi:menu.view.contentWidth.medium'
                                        ),
                                        accelerator: 'CmdOrCtrl+2',
                                        click: () => {
                                            windowManager.sendWebsocketMessageToFocusedWindow(
                                                {
                                                    type: 'H5P_SET_WIDTH_LIMIT',
                                                    payload: {
                                                        limit: 2
                                                    }
                                                }
                                            );
                                        }
                                    },
                                    {
                                        label: i18next.t(
                                            'lumi:menu.view.contentWidth.wide'
                                        ),
                                        accelerator: 'CmdOrCtrl+3',
                                        click: () => {
                                            windowManager.sendWebsocketMessageToFocusedWindow(
                                                {
                                                    type: 'H5P_SET_WIDTH_LIMIT',
                                                    payload: {
                                                        limit: 3
                                                    }
                                                }
                                            );
                                        }
                                    },
                                    {
                                        label: i18next.t(
                                            'lumi:menu.view.contentWidth.full'
                                        ),
                                        accelerator: 'CmdOrCtrl+0',
                                        click: () => {
                                            windowManager.sendWebsocketMessageToFocusedWindow(
                                                {
                                                    type: 'H5P_SET_WIDTH_LIMIT',
                                                    payload: {
                                                        limit: 0
                                                    }
                                                }
                                            );
                                        }
                                    }
                                ]
                            }
                          : undefined,
                      isMac && h5pOpenInWindow
                          ? { type: 'separator' }
                          : undefined,
                      isMac
                          ? {
                                label: i18next.t(
                                    'lumi:menu.view.togglefullscreen'
                                ),
                                role: 'togglefullscreen'
                            }
                          : undefined
                  ].filter((e) => e !== undefined)
              }
          ];
}

import electron from 'electron';
import WindowManager from '../state/WindowManager';
import SocketIO from 'socket.io';
import H5PPlayerMenu from './H5PPlayerMenu';

export default function menuFactory(
    path: string,
    windowManager: WindowManager
): void {
    switch (path) {
        case '/':
        default:
            electron.Menu.setApplicationMenu(
                electron.Menu.buildFromTemplate(H5PPlayerMenu(windowManager))
            );
    }
}

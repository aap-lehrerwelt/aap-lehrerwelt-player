import electron, { dialog } from 'electron';
import i18next from 'i18next';
import WindowManager from '../state/WindowManager';

export default function (windowManager: WindowManager): any {
    return {
        label: i18next.t('lumi:menu.help.label'),
        submenu: [
            process.env.DEV_TOOLS === 'enabled' &&
            windowManager?.getFocusedWindow()
                ? {
                      click: () => {
                          windowManager
                              .getFocusedWindow()
                              ?.browserWindow.webContents.openDevTools();
                      },
                      label: i18next.t('lumi:menu.help.toggle_developer_tools')
                  }
                : undefined,
            process.env.DEV_TOOLS === 'enabled' &&
            windowManager?.getFocusedWindow()
                ? { type: 'separator' }
                : undefined,
            {
                label: i18next.t('lumi:menu.help.license'),
                click: () => {
                    windowManager.createSpecialWindow('license.html');
                }
            },
            {
                label: i18next.t('lumi:privacy_policy.title'),
                click: () => {
                    windowManager.createSpecialWindow('privacy.html');
                }
            },
            {
                label: i18next.t('lumi:menu.help.licenses'),
                click: () => {
                    windowManager.createSpecialWindow('DISCLAIMER.html');
                }
            },
            {
                label: i18next.t('lumi:menu.help.contact'),
                click: () => {
                    dialog.showMessageBox(
                        windowManager.getFocusedWindow().browserWindow,
                        {
                            message: i18next.t(
                                'lumi:menu.help.contactInformation'
                            ),
                            type: 'info',
                            title: i18next.t('lumi:menu.help.contact')
                        }
                    );
                }
            },
            { label: i18next.t('lumi:menu.help.about'), role: 'about' }
        ].filter((e) => e !== undefined)
    };
}

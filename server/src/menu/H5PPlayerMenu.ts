import electron from 'electron';
import i18next from 'i18next';

import helpMenu from './helpMenu';
import editMenu from './editMenu';
import macMenu from './macMenu';
import windowMenu from './windowMenu';
import viewMenu from './viewMenu';
import WindowManager from '../state/WindowManager';

export default (windowManager: WindowManager) => [
    ...macMenu(windowManager),
    {
        label: i18next.t('lumi:menu.file.label'),
        submenu: [
            {
                accelerator: 'CmdOrCtrl+O',
                click: () => {
                    electron.dialog
                        .showOpenDialog({
                            filters: [
                                {
                                    extensions: ['h5p'],
                                    name: 'HTML 5 Package'
                                }
                            ],
                            properties: ['openFile', 'multiSelections']
                        })
                        .then(({ filePaths }) => {
                            for (const file of filePaths) {
                                windowManager.openH5P(
                                    file,
                                    process.platform === 'win32'
                                );
                            }
                        });
                },
                label: i18next.t('lumi:menu.h5pPlayer.open')
            },
            { type: 'separator' } as any,
            process.platform !== 'darwin'
                ? ({ type: 'separator' } as any)
                : undefined,
            process.platform !== 'darwin'
                ? {
                      label: i18next.t('lumi:menu.mac.preferences'),
                      click: () => {
                          windowManager.sendWebsocketMessageToFocusedWindow({
                              payload: {
                                  show: true,
                                  section: 'general'
                              },
                              type: 'SHOW_SETTINGS'
                          });
                      }
                  }
                : undefined,
            process.platform !== 'darwin'
                ? ({ type: 'separator' } as any)
                : undefined,
            process.platform === 'darwin'
                ? {
                      label: i18next.t('lumi:menu.file.close'),
                      role: 'close'
                  }
                : {
                      label: i18next.t('lumi:menu.quit'),
                      role: 'quit'
                  }
        ].filter((e) => e !== undefined)
    },
    editMenu(),
    ...viewMenu(windowManager),
    ...windowMenu(windowManager),
    helpMenu(windowManager)
];

import i18next from 'i18next';
import WindowManager from '../state/WindowManager';
const isMac = process.platform === 'darwin';

export default function (windowManager: WindowManager): any {
    return isMac
        ? [
              {
                  label: 'Player für interaktive Übungen',
                  submenu: [
                      {
                          label: i18next.t('lumi:menu.help.about'),
                          role: 'about'
                      },
                      { type: 'separator' },
                      {
                          label: i18next.t('lumi:menu.mac.preferences'),
                          click: () => {
                              windowManager.sendWebsocketMessageToFocusedWindow(
                                  {
                                      payload: {
                                          show: true,
                                          section: 'general'
                                      },
                                      type: 'SHOW_SETTINGS'
                                  }
                              );
                          }
                      },
                      { type: 'separator' },
                      {
                          label: i18next.t('lumi:menu.mac.services'),
                          role: 'services'
                      },
                      { type: 'separator' },
                      { label: i18next.t('lumi:menu.mac.hide'), role: 'hide' },
                      {
                          label: i18next.t('lumi:menu.mac.hideothers'),
                          role: 'hideothers'
                      },
                      {
                          label: i18next.t('lumi:menu.mac.unhide'),
                          role: 'unhide'
                      },
                      { type: 'separator' },
                      {
                          label: i18next.t('lumi:menu.quit'),
                          role: 'quit'
                      }
                  ]
              }
          ]
        : [];
}

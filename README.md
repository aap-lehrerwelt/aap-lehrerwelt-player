# Lumi Player

Lumi Player is a Desktop App that allows you to view H5P files in your desktop
system (macOS and Windows).

**Tech stack:**

- [Electron](https://electronjs.org) - desktop application wrapper
- [NodeJS](https://nodejs.org/) - backend
- [Express](https://expressjs.com/) - webserver
- [socket.io](http://socket.io) - bidirectional communication between browser
  window and server
- [React.js](https://reactjs.org/) - user interface technology
- [Material UI](https://www.material-ui.com) - user interface visuals
- [Redux](https://redux.js.org/) - user interface state management
- [h5p-nodejs-library](https://docs.lumi.education) - H5P support

Lumi Player is being developed by the [Lumi Education GbR Jan Philip
Schellenberg und Sebastian Rettig](https://lumi.education) under contract to
[AAP Lehrerwelt GmbH](https://lehrerwelt.de).

## Contributing

Please read [CONTRIBUTING.md](./.github/CONTRIBUTING.md) for details on our code
of conduct, and the process for submitting pull requests to us. Lumi has adopted
the code of conduct defined by the Contributor Covenant. It can be read in full
[here](./.github/CODE-OF-CONDUCT.md).

### Getting in touch

Contact the developers as [c@lumi.education](mailto:c@lumi.education).

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE v3 License. See
the [LICENSE](LICENSE.txt) file for details

## Creating the .ico file

Install ImageMagick with legacy mode

```sh
convert logo.png -define icon:auto-resize=16,24,32,40,48,64,96,128,256 -compress zip out.ico
```

## Building und deploying a release

### macOS

- Set these environment variables:
  - BITBUCKET_TOKEN (= app password in Bitbucket for a user)
  - BITBUCKET_USERNAME (if you push to a repo that's not owned by you)

```sh
npm run publish:mac
```

### Manual uploads to Bitbucket

```sh
curl -s -u username:password -X POST https://api.bitbucket.org/2.0/repositories/aap-lehrerwelt/aap-lehrerwelt-player/downloads -F files=@Player-für-interaktive-Übungen-1.0.1.exe
```

# Vorgehen für Signieren der Dateien

## Windows

### Zertifikat installieren

- proCertum CardManager installieren
- CardManager starten und auf "Common profile" klicken
- ggf. PIN erneut eingeben
- Zertifikat in Liste auswählen und auf "Show certificate details" klicken
- "Zertifikat installieren" wählen
- "Lokaler Computer" wählen und "weiter"
- "Alle Zertifikate in folgendem Speicher speicher" wählen und dann "Durchsuchen"
- "Eigene Zertifikate" wählen
- "Ok", "Weiter" ... Dialog beenden
- das Gleiche für aktuellen Nutzer durchführen
- Wichtig: Shell neu starten! (und evtl. Stick herausziehen und neu einstecken)

### Bauen

- `npm run build:win` ausühren und mehrfach PIN eingeben (nötig, da sowohl die Dateien im Installer als auch der Installer signiert werden müssen und zwei verschiedene Signierungen mit SHA1 und SHA256 vorgenommen werden)
- zum sofortigen Hochladen: `BITBUCKET_TOKEN=XXX BITBUCKET_USERNAME=YYY npm run publish:win`

### Manuelles Signieren (zum Testen des Zertifikats / der Installation)

Windows 10 SDK installieren: https://developer.microsoft.com/en-us/windows/downloads/windows-sdk/

```sh
"C:\Program Files (x86)\Windows Kits\10\bin\10.0.22000.0\x64\signtool.exe" sign /n "AAP Lehrerwelt GmbH" /t "http://time.certum.pl/" /fd SHA256 /v "e:\Source\lumieducation\LumiPlayer\dist\Player für interaktive Übungen Setup 0.2.0.exe"
```

Dabei 10.0.22000.0 mit aktueller Version des Downloads ersetzen und Pfade anpassen

## macOS

Alles mit einem Apple-Entwickler-Account ausführen.

- Developer ID App-Zertifikat von https://developer.apple.com herunterladen (nicht Mac Development!) und installieren
- Im Developer Account App-Passwort erzeugen (https://appleid.apple.com)
- In Schlüsselbundverwaltung den Namen des Zertifikat kopieren ("AAP Lehrerwelt (XXXXXXXXXX)")
- Bei erstmaliger Verwendung: "sudo xcode-select -r" (löst Fehler bei Notarizing: xcrun: error: unable to find utility "altool", not a developer tool or in PATH)
- CSC_NAME="Name des Zertifikats" APPLEID=appleid@des.entwicklers APPLEIDPASSWORD=app-passwort-das-oben-erstellt-wurde npm run build:mac
- CSC_NAME="AAP Lehrerwelt GmbH (QEAC4LQ23U)" APPLEID=xxx@yyy.de APPLEIDPASSWORD=xxxx-xxxx-xxxx-xxxx BITBUCKET_USERNAME=xxx BITBUCKET_TOKEN=YYYYYYYYYYYYYYYY npm run publish:mac
- ggf. Passwort eingeben